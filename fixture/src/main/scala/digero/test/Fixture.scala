package digero.test

import java.io.{File, InputStream, InputStreamReader}
import java.nio.charset.StandardCharsets
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.{Files, Paths}
import scala.util.Using

trait Fixture:
  def fixture(name: String): String = Fixture.getString(name)
  def fixturePath(name: String): String = Fixture.getPath(name)
  def fixtureFile(name: String): File = Fixture.getFile(name)
  def fixtureStream(name: String): InputStream = Fixture.getStream(name)
  def fixtureWrite(content: String, path: String): Unit = Fixture.write(content, path)

  def temporaryFile(name: String, suffix: String = "fixture"): File =
    File.createTempFile(name,
      if suffix.startsWith(".") then suffix else s".${suffix}",
      temporaryDirectory()
    )

  private def temporaryDirectory(): File = File("build/tmp/test")
  
object Fixture:
  private def getPath(fixture: String): String = getFile(fixture).getAbsolutePath

  private def getFile(fixture: String): File =
    val path = if fixture.startsWith("/")
    then fixture
    else "/" + fixture
    val resource = classOf[Fixture].getResource(path)
    assert(resource != null)
    new File(resource.getFile)

  private def getStream(fixture: String): InputStream =
    val path = if (fixture.startsWith("/")) fixture
    else "/" + fixture
    val resource = classOf[Fixture].getResource(path)
    assert(resource != null, s"missing fixture ${fixture}")
    resource.openStream

  private def getString(fixture: String): String =
    Using(new InputStreamReader(getStream(fixture), UTF_8)) { reader =>
      val buffer = new Array[Char](8192)
      val builder = StringBuilder()
      var n = 0
      while {n = reader.read(buffer); n >= 0}
        do
          builder.appendAll(buffer, 0, n)
      builder.toString
    }.get

  private def write(content: String, path: String): Unit =
    Files.write(Paths.get(path), content.getBytes(UTF_8))

