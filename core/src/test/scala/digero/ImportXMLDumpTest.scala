package digero

import digero.test.Fixture
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{Tag, Test}

import scala.util.Using

@Tag("integration")
class ImportXMLDumpTest extends Fixture:

  @Test
  def testImportXMLDump(): Unit =
    val outputPath = temporaryFile("import-xml.parquet")

    val inputs = Seq(
      fixtureFile("enwiktionary-20150224-pages-articles-multistream.xml.bz2"),
      fixtureFile("enwiktionary-20150224-pages-articles-multistream-index.txt.bz2")
    ).map(_.getAbsolutePath)

    runXMLTask(ImportXMLDump(),
      TaskArguments(Seq(
        inputs.mkString(","),
        outputPath.getAbsolutePath)
    ))

    assertThat(outputPath).exists()

    Using.resource(SparkRunner("testImportXMLDump", null)) { runner =>
      val dataset = runner.session.read.parquet(outputPath.getAbsolutePath)
      assertThat(dataset.count()).isEqualTo(300)
    }

    println(s"wrote file to ${outputPath}")