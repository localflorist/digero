package digero

import digero.DataFrameTask
import digero.test.Fixture
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

import java.io.File

@Tag("integration")
class ProcessHTMLDumpTest extends Fixture:

  @ParameterizedTest(name = "runDatasetTask({0}, {1})")
  @CsvSource(value = Array("enwiktionary-NS0-test-ENTERPRISE-HTML.json.tar.gz, 15"))
  def testProcess(dumpResource: String, expectedCount: Int): Unit =
    val task: DataFrameTask[Unit] = (session: SparkSession, input: DataFrame, _) => {
      assertThat(input.count()).isEqualTo(expectedCount)
    }
    runHTMLTask(task, fixtureFile(dumpResource).getAbsolutePath)


