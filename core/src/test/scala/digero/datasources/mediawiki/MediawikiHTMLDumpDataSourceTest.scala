package digero.datasources.mediawiki

import digero.HTMLDumpSchema
import digero.test.Fixture
import digero.codecs.TarGzCodec
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{BeforeEach, Tag, Test}

import scala.util.Using

@Tag("integration")
class MediawikiHTMLDumpDataSourceTest extends Fixture:
  private var testConf: SparkConf = _

  @BeforeEach def setUp(): Unit =
    testConf = new SparkConf(false)
        .set("io.compression.codecs", classOf[TarGzCodec].getName)
        .setAppName("test")
        .setMaster("local")

  @Test
  def testLoadData(): Unit =
    Using.resource(SparkSession.builder().config(testConf).getOrCreate()) { session =>
      val dump: String = fixturePath("enwiktionary-NS0-test-ENTERPRISE-HTML.json.tar.gz")
      val dataset: DataFrame = session.read.schema(HTMLDumpSchema.input).format("json").load(dump)
      assertThat(dataset.count).isEqualTo(15)
    }
