package digero.codecs

import digero.test.Fixture
import org.apache.hadoop.io.compress.{CompressionCodec, CompressionInputStream}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import java.io.{IOException, InputStream}

class TarGzCodeTest extends Fixture:
  @Test
  def testRead(): Unit =
    val is = fixtureStream("enwiktionary-NS0-test-ENTERPRISE-HTML.json.tar.gz")
    val codec = new TarGzCodec
    val inputStream = codec.createInputStream(is)
    val bytes = inputStream.readAllBytes
    assertThat(bytes.length).isEqualTo(106274)
