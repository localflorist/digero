package digero.parser

import digero.test.Fixture
import digero.model.RawPage
import org.apache.spark.sql.sources.{EqualTo, In, IsNotNull, IsNull}
import org.assertj.core.api.Assertions.{assertThat, assertThatThrownBy}
import org.junit.jupiter.api.Test

import java.io.InputStream
import java.time.temporal.TemporalField

class MediawikiParserTest extends Fixture:

  @Test
  def testParseTitle(): Unit =
    val page = parse(fixtureStream("welcome.xml"))
    assertThat(page.title).isEqualTo("Wiktionary:Welcome, newcomers")

  @Test
  def testParseId(): Unit =
    val page = parse(fixtureStream("welcome.xml"))
    assertThat(page.id).isEqualTo(6L)

  @Test
  def testParseText(): Unit =
    val page = parse(fixtureStream("welcome.xml"))
    assertThat(page.text).isNotEmpty

  @Test
  def testParseNamespace(): Unit =
    val page = parse(fixtureStream("welcome.xml"))
    assertThat(page.namespace).isEqualTo(4)

  @Test
  def testParseRevisionId(): Unit =
    val page = parse(fixtureStream("welcome.xml"))
    assertThat(page.revisionId).isEqualTo(24557508L)

  @Test
  def testParseContributorId(): Unit =
    val page = parse(fixtureStream("welcome.xml"))
    assertThat(page.contributorId).isEqualTo(14633L)

  @Test
  def testParseEmptyContributorId(): Unit =
    val page = parse(fixtureStream("empty_contributor_id.xml"))
    assertThat(page.contributorId).isEqualTo(-1L)

  @Test
  def testParseRedirect(): Unit =
    val page = parse(fixtureStream("redirect.xml"))
    assertThat(page.redirect).isEqualTo("with a grain of salt")

  @Test
  def testParseTimestamp(): Unit =
    val page = parse(fixtureStream("deleted.xml"))
    assertThat(page.timestamp).isEqualTo("2017-06-06T20:21:09Z")

  @Test
  def testParseContentModel(): Unit =
    val page = parse(fixtureStream("welcome.xml"))
    assertThat(page.contentModel).isEqualTo("wikitext")

  @Test def testUnsupportedVersion(): Unit =
    assertThatThrownBy(() => {
      parse(fixtureStream("future_version.xml"))
    }).hasMessageContaining("Unsupported dump version: 99.99")

  @Test
  def testCurrentVersion(): Unit =
    parse(fixtureStream("current_version.xml"))

  @Test
  def testNamespaceFilterEqual(): Unit =
    val parser = MediawikiParser(fixtureStream("filter.xml"), EqualTo("namespace", 1))
    val page = RawPage()
    assertThat(parser.next(page)).isTrue
    assertThat(page.namespace).isEqualTo(1)
    assertThat(page.title).isEqualTo("page 2")
    assertThat(parser.next(page)).isFalse

  @Test
  def testNamespaceFilterIn(): Unit =
    val parser = MediawikiParser(fixtureStream("filter.xml"), In("namespace", Array(0, 1)))
    val page = RawPage()
    assertThat(parser.next(page)).isTrue
    assertThat(page.namespace).isEqualTo(0)
    assertThat(parser.next(page)).isTrue
    assertThat(page.namespace).isEqualTo(1)
    assertThat(parser.next(page)).isTrue
    assertThat(page.namespace).isEqualTo(0)
    assertThat(parser.next(page)).isTrue
    assertThat(page.namespace).isEqualTo(0)
    assertThat(parser.next(page)).isFalse

  @Test
  def testRedirectFilterNull(): Unit =
    val parser = MediawikiParser(fixtureStream("filter.xml"), IsNull("redirect"))
    val page = RawPage()
    assertThat(parser.next(page)).isTrue
    assertThat(page.title).isEqualTo("page 1")
    assertThat(parser.next(page)).isTrue
    assertThat(page.title).isEqualTo("page 2")
    assertThat(parser.next(page)).isTrue
    assertThat(page.title).isEqualTo("page 4")
    assertThat(parser.next(page)).isTrue
    assertThat(parser.next(page)).isFalse

  @Test
  def testRedirectFilterNotNull(): Unit =
    val parser = MediawikiParser(fixtureStream("filter.xml"), IsNotNull("redirect"))
    val page = RawPage()
    assertThat(parser.next(page)).isTrue
    assertThat(page.title).isEqualTo("page 3")
    assertThat(parser.next(page)).isFalse

  private def parse(inputStream: InputStream):RawPage =
    val parser = MediawikiParser(inputStream)
    val page = RawPage()
    assertThat(parser.next(page)).isTrue
    page
