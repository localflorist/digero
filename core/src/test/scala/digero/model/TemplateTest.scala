package digero.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.{BeforeEach, Test}

import java.util
import org.scalatest.matchers.should.Matchers.*

class TemplateTest:
  var subject: Template = _

  @BeforeEach def setUp(): Unit =
    val parameters = new util.HashMap[String, String]
    parameters.put("1", "Foo")
    parameters.put("2", "Bar")
    subject = new Template("test", parameters)

  @Test def testGetParameter(): Unit =
    subject.getParameter(1) should contain("Foo")
    subject.getParameter(2) should contain("Bar")
    subject.getParameter(3) shouldBe empty

  @Test def testGetByParameterKey(): Unit =
    subject.getParameter("1") should contain("Foo")
    subject.getParameter("2") should contain("Bar")
    subject.getParameter("3") shouldBe empty

  @Test def testGetAnonymousParameters(): Unit =
    val parameters = new util.HashMap[String, String]
    parameters.put("5", "Foo")
    parameters.put("key", "value")
    parameters.put("10", "Baz")
    parameters.put("3", "Bar")
    val template = new Template("test", parameters)
    assertThat(template.getAnonymousParameters).containsExactly("Bar", "Foo", "Baz")

  @Test def testAssumesOneBasedIndex(): Unit =
    assertThrows(classOf[AssertionError], () => subject.getParameter(0))

  @Test def testConstructFromParametersWithGaps(): Unit =
    val parameters = new util.HashMap[String, String]
    parameters.put("1", "Foo")
    parameters.put("3", "Bar")
    val template = new Template("test", parameters)
    template.getParameter(1) should contain("Foo")
    template.getParameter(2) shouldBe empty
    template.getParameter(3) should contain("Bar")
