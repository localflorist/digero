package digero

import scala.util.Using

/**
 * Runner class for a [[DataFrameTask]], using a previously parsed dump.
 * See [[digero.ProcessDump]] for the runner class to process a dump.
 */
@main
def ProcessDataFrame(className: String, file: String, rest: String*): Unit =
  val task: DataFrameTask[_] = Task.getTask(className)

  Using.resource(SparkRunner(task)) { runner =>
    val input = if file.endsWith(".json") || file.endsWith(".jsonl") || file.endsWith(".ndjson") then
      runner.session.read.json(file)
    else
      runner.session.read.parquet(file)

    task.process(runner.session, input, TaskArguments(rest.toArray))
  }
