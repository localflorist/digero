package digero.rdd

import digero.model.RawPage
import digero.parser.MediawikiParser
import digero.rdd.PageReader.getInputStreamAtOffset
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.io.compress.bzip2.CBZip2InputStream
import org.apache.hadoop.mapreduce.lib.input.FileSplit
import org.apache.hadoop.mapreduce.{InputSplit, RecordReader, TaskAttemptContext}
import org.apache.spark.sql.sources.Filter

import java.io.{ByteArrayInputStream, InputStream, SequenceInputStream}
import java.nio.charset.StandardCharsets.UTF_8
import scala.math.{max, min}

class PageReader(val filters: Filter*) extends RecordReader[String, RawPage]:
  private var length = 0L
  private var offset = 0L
  private var mediawikiParser: MediawikiParser = _
  private var fileIn: FSDataInputStream = _
  private val currentPage = RawPage()
  private var currentKey: String = _

  override def initialize(split: InputSplit, context: TaskAttemptContext): Unit =
    val fileSplit = split.asInstanceOf[FileSplit]
    val job = context.getConfiguration
    val file = fileSplit.getPath
    val fs = file.getFileSystem(job)

    offset = fileSplit.getStart
    length = fileSplit.getLength
    fileIn = fs.open(file)
    mediawikiParser = MediawikiParser(getInputStreamAtOffset(fileIn, offset), filters: _*)

  override def nextKeyValue(): Boolean =
    if mediawikiParser.next(currentPage) then
      currentKey = currentPage.title
      true
    else
      false

  override def getCurrentKey: String = currentKey
  override def getCurrentValue: RawPage = currentPage

  override def getProgress: Float =
    if fileIn == null then
      0
    else
      max(0f, min(1.0f, (fileIn.getPos - offset) / length.toFloat))

  override def close(): Unit =
    if mediawikiParser != null then mediawikiParser.close()
    if fileIn != null then fileIn.close()


object PageReader:
  private val MEDIAWIKI_OPENING = "<mediawiki>"
  private val MEDIAWIKI_CLOSING = "</mediawiki>"

  def getInputStreamAtOffset(stream: FSDataInputStream, offset: Long): InputStream =
    stream.seek(offset + 2) // skip past 'BZ' header

    if (offset == 0)
      CBZip2InputStream(stream)
    else
      SequenceInputStream(
        ByteArrayInputStream(MEDIAWIKI_OPENING.getBytes(UTF_8)),
        SequenceInputStream(
          new CBZip2InputStream(stream) {
            override def skipToNextMarker(marker: Long, markerBitLength: Int) = false
          },
          ByteArrayInputStream(MEDIAWIKI_CLOSING.getBytes(UTF_8))
        )
      )
