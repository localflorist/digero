package digero

import digero.HTMLDumpSchema.parseTimestamp
import digero.spark.{getOpt, getSeqOpt}
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.functions.{col, length, sha1}
import org.apache.spark.sql.{DataFrame, Encoder, Row, SaveMode, SparkSession}

import java.io.File

class ImportHTMLDump extends DataFrameTask[File]:

  given encoder: Encoder[Row] = ExpressionEncoder(HTMLDumpSchema.simplifiedOutput)

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    log.info(s"ImportHTMLDump($arguments)")
    require(arguments.length >= 1, "no arguments")

    val output = arguments.outputFile
    require(!output.getPath.isBlank, "need output dir") // Spark will happily trash the local directory :(

    (for
      row <- input
      name: String <- row.getOpt("name")
      identifier: Long = row.getAs("identifier")
      dateModified = parseTimestamp(row.getAs("date_modified"))
      namespace: Row = row.getAs("namespace")
      namespaceIdentifier: Int = namespace.getAs("identifier")
      articleBody: Row = row.getAs("article_body")
      html: String = articleBody.getAs("html")
      redirects: Option[Seq[Row]] = row.getSeqOpt("redirects")
      redirectNames = redirects.map(_.map(_.getAs[String]("name")))
    yield Row(identifier, name, namespaceIdentifier, dateModified, redirectNames.orNull, html))
      .repartition(200, col("id")) // this produces ~80MB files
      .withColumn("content_hash", sha1(col("content")))
      .withColumn("content_length", length(col("content")))
      .write
      .mode(SaveMode.Overwrite)
      .option("compression", "snappy")
      .option("header", "true")
      .parquet(output.getPath)

    output