package digero

import org.apache.commons.logging.LogFactory

import scala.util.Using

@main
def ProcessHTMLDump(className: String, rest: String*): Unit =
  val task = Task.getTask[DataFrameTask[_]](className)
  runHTMLTask(task, rest: _*)

def runHTMLTask(task: DataFrameTask[_], args: String*): Unit =
  val log = LogFactory.getLog("ProcessHTMLDump")
  val (inputs, rest) = args.partition(x => x.endsWith(".ndjson") || x.endsWith(".json.tar.gz"))

  log.info(s"runHTMLTask(input=${inputs}, args=${rest})")

  Using.resource(SparkRunner(task)) { runner =>
    val dataset = runner.session.read.schema(HTMLDumpSchema.input).json(inputs: _*)
    task.process(runner.session, dataset, TaskArguments(rest.toArray))
  }
