package digero

import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 * A task which operates on a [[DataFrame]].
 */
trait DataFrameTask[Output] extends Task:
  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): Output
  