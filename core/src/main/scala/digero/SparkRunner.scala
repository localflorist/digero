package digero

import digero.codecs.TarGzCodec
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession

import java.nio.file.Paths

case class SparkRunner(appName: String, kryoRegistrator: String) extends AutoCloseable:

  val conf: SparkConf = new SparkConf()
    .setAppName(appName)
    .setMaster("local[*]")
    .set("spark.local.dir", Paths.get("./tmp").toAbsolutePath.normalize.toString)
    .set("io.compression.codecs", classOf[TarGzCodec].getName)
    .set("spark.sql.datetime.java8API.enabled", "true")
    .set("spark.sql.objectHashAggregate.sortBased.fallbackThreshold", "16384")
    .set("spark.sql.parquet.columnarReaderBatchSize", "2048")
    .set("spark.sql.parquet.enableVectorizedReader", "false")

  if kryoRegistrator != null then
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.set("spark.kryo.registrator", kryoRegistrator)

  val session: SparkSession = SparkSession.builder.config(conf).getOrCreate()
  val context:SparkContext = session.sparkContext

  override def close(): Unit =
    session.close()
    SparkSession.clearDefaultSession()


object SparkRunner:
  def apply(task: Task): SparkRunner =
    new SparkRunner(task.getClass.getSimpleName, task.kryoRegistrator)





