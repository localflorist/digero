package digero

import digero.model.RawPage
import digero.rdd.MediawikiDumpInputFormat
import org.apache.commons.logging.LogFactory
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat.SPLIT_MAXSIZE

import scala.util.Using

@main
def ProcessXMLDump(className: String, rest: String*): Unit = runXMLTask(Task.getTask(className), TaskArguments(rest))

def runXMLTask(task: Task, args: TaskArguments): Unit =
  val log = LogFactory.getLog("ProcessDump")
  log.info(s"processing $args")
  require(args.nonEmpty)

  Using.resource(SparkRunner(task)) { runner =>
    task match
      case dataframeTask: DataFrameTask[?] =>
        log.info("using DataFrame for processing")

        val reader = runner.session.read.format("mediawiki").option("groupSize", 8)

        val dumps = args.head.split(",", 2)
        val dataset = reader.load(dumps: _*)

        dataframeTask.process(runner.session, dataset, args.skipFirst)

      case rdd: RDDTask[?] =>
        log.info("using RDD[String, RawPage] for processing")

        val configuration = Configuration(false)
        configuration.setLong(SPLIT_MAXSIZE, Long.MaxValue)

        val input = runner.context.newAPIHadoopFile(
          args(0),
          classOf[MediawikiDumpInputFormat],
          classOf[String],
          classOf[RawPage],
          configuration
        ).setName(task.getClass.getSimpleName)

        rdd.process(runner.context, input, args.skipFirst)
  }
