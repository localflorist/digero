package digero.datasources.mediawiki

import digero.XMLDumpSchema
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileStatus, Path}
import org.apache.hadoop.io.compress.bzip2.CBZip2InputStream
import org.apache.hadoop.mapreduce.lib.input.FileSplit
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.paths.SparkPath
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.expressions.Expression
import org.apache.spark.sql.catalyst.{FileSourceOptions, InternalRow}
import org.apache.spark.sql.connector.catalog.Table
import org.apache.spark.sql.connector.read.*
import org.apache.spark.sql.connector.write.{LogicalWriteInfo, WriteBuilder}
import org.apache.spark.sql.execution.datasources.binaryfile.BinaryFileFormat
import org.apache.spark.sql.execution.datasources.v2.*
import org.apache.spark.sql.execution.datasources.{FileFormat, FilePartition, PartitionedFile, PartitioningAwareFileIndex}
import org.apache.spark.sql.internal.SQLConf
import org.apache.spark.sql.sources.Filter
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.util.CaseInsensitiveStringMap
import org.apache.spark.util.SerializableConfiguration

import java.io.{IOException, InputStream}
import scala.io.Source
import scala.jdk.CollectionConverters.*
import scala.util.Using

class MediawikiDumpDataSourceV2 extends FileDataSourceV2:
  override def fallbackFileFormat: Class[_ <: FileFormat] = classOf[MediawikiDumpFileFormat]
  override def shortName() = "mediawiki"

  override protected def getTable(options: CaseInsensitiveStringMap): Table =
    val paths = getPaths(options)
    val tableName = getTableName(options, paths)
    val optionsWithoutPaths = getOptionsWithoutPaths(options)
    MediawikiTable(tableName, sparkSession, optionsWithoutPaths, paths, None, fallbackFileFormat)

  override protected def getTable(options: CaseInsensitiveStringMap, schema: StructType): Table =
    val paths = getPaths(options)
    val tableName = getTableName(options, paths)
    val optionsWithoutPaths = getOptionsWithoutPaths(options)
    MediawikiTable(tableName, sparkSession, optionsWithoutPaths, paths, Option(schema), fallbackFileFormat)

  override def inferSchema(options: CaseInsensitiveStringMap): StructType = XMLDumpSchema.output

private case class MediawikiTable(
  name: String,
  sparkSession: SparkSession,
  options: CaseInsensitiveStringMap,
  paths: Seq[String],
  userSpecifiedSchema: Option[StructType],
  fallbackFileFormat: Class[_ <: FileFormat]) extends FileTable(sparkSession, options, paths, userSpecifiedSchema):

  override def inferSchema(files: Seq[FileStatus]): Option[StructType] = Option.empty
  override def formatName: String = "MEDIAWIKI"

  override def newScanBuilder(options: CaseInsensitiveStringMap): ScanBuilder =
    MediawikiScanBuilder(sparkSession, fileIndex, dataSchema, options)

  override def newWriteBuilder(info: LogicalWriteInfo): WriteBuilder = ???


private class MediawikiScanBuilder(
  sparkSession: SparkSession,
  fileIndex: PartitioningAwareFileIndex,
  dataSchema: StructType,
  options: CaseInsensitiveStringMap) extends FileScanBuilder(sparkSession, fileIndex, dataSchema):

  override def build(): Scan =
    MediawikiScan(sparkSession, fileIndex, dataSchema, dataSchema, readPartitionSchema(), options, pushedDataFilters)

  override def pushDataFilters(dataFilters: Array[Filter]): Array[Filter] = {
    val map: Map[Boolean, Array[Filter]] = dataFilters.groupBy { filter =>
      filter.references.contains("namespace") || filter.references.contains("redirect")
    }
    map.getOrElse(true, Array.empty[Filter])
  }

private case class MediawikiScan(
   sparkSession: SparkSession,
   fileIndex: PartitioningAwareFileIndex,
   dataSchema: StructType,
   readDataSchema: StructType,
   readPartitionSchema: StructType,
   options: CaseInsensitiveStringMap,
   pushDownFilters: Array[Filter] = Array.empty,
   partitionFilters: Seq[Expression] = Seq.empty,
   dataFilters: Seq[Expression] = Seq.empty)
  extends FileScan:

  private lazy val inputPartitions: Array[InputPartition] = doPlanInputPartitions()

  override def createReaderFactory(): PartitionReaderFactory =
    val caseSensitiveMap = options.asCaseSensitiveMap.asScala.toMap
    val hadoopConf = sparkSession.sessionState.newHadoopConfWithOptions(caseSensitiveMap)
    val broadcastedConf = sparkSession.sparkContext.broadcast(SerializableConfiguration(hadoopConf))

    MediawikiPartitionReaderFactory(
      sparkSession.sessionState.conf,
      broadcastedConf,
      dataSchema,
      readDataSchema,
      readPartitionSchema,
      dataFilters,
      pushDownFilters
    )

  // NB: this is called twice: once to get the schema and again later, so cache results
  // https://github.com/vertica/spark-connector/issues/171
  override def planInputPartitions(): Array[InputPartition] = inputPartitions

  private def doPlanInputPartitions(): Array[InputPartition] =
    val partitions = super.planInputPartitions()
    val partitionedFiles = partitions.flatMap {
      case fp: FilePartition => fp.files
      case _ => Array[PartitionedFile]()
    }
    val groupSize = options.getInt("groupSize", 1)
    assert(groupSize >= 1)

    partitionedFiles match
      case Array(dump, index, _*) =>
        readPartitionedFiles(indexFile = index.filePath.toPath, dump = dump.filePath.toPath)
          .grouped(groupSize)
          .zipWithIndex
          .map { (group, index) => FilePartition(index, files = group) }
          .toArray
      case _ => partitions

  private def readPartitionedFiles(indexFile: Path, dump: Path): Array[PartitionedFile] =
    logInfo(s"readPartitionedFiles: indexFile=$indexFile dump=$dump")
    val fs = dump.getFileSystem(Configuration())
    val offsetPairs = Using.resource(fs.open(indexFile)) { indexStream =>
       collectOffsets(indexStream, fs.getFileStatus(dump).getLen)
        .sliding(2, 1)
        .map(s => (s.head, s(1)))
        .toArray
    }
    for
      (start, end) <- offsetPairs
      length = end - start
    yield PartitionedFile(InternalRow(), SparkPath.fromPath(dump), start, length)

  private def collectOffsets(stream: InputStream, fileLength: Long): Iterator[Long] =
    if stream.skip(2) != 2 then throw IOException()
    val offsets = for
      line <- Source.fromInputStream(CBZip2InputStream(stream)).getLines()
      parts = line.split(":", 3)
      firstPart <- parts.headOption
    yield firstPart

    offsets.distinct.map(_.toLong) ++ Seq(fileLength)

private case class MediawikiPartitionReaderFactory(
   sqlConf: SQLConf,
   broadcastedConf: Broadcast[SerializableConfiguration],
   dataSchema: StructType,
   readDataSchema: StructType,
   partitionSchema: StructType,
   dataFilters: Seq[Expression],
   pushDownFilter: Array[Filter]) extends FilePartitionReaderFactory:

  override def options: FileSourceOptions = FileSourceOptions(Map.empty)

  override def buildReader(partitionedFile: PartitionedFile): PartitionReader[InternalRow] =
    val fileSplit = FileSplit(
      partitionedFile.filePath.toPath,
      partitionedFile.start,
      partitionedFile.length,
      Array.empty)
    MediawikiPartitionReader(fileSplit, pushDownFilter)

class MediawikiDumpFileFormat extends BinaryFileFormat {
}
