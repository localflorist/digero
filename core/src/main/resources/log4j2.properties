#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Set everything to be logged to the console
appender.console.type = Console
appender.console.name = STDOUT
appender.console.layout.type = PatternLayout
appender.console.layout.pattern = %d{yy/MM/dd HH:mm:ss} %p %c{1}: %m%n

rootLogger = INFO, STDOUT

# Set the default spark-shell log level to WARN. When running the spark-shell, the
# log level for this class is used to overwrite the root logger's log level, so that
# the user can have different defaults for the shell and regular Spark apps.
logger.repl.name=org.apache.spark.repl.Main
logger.repl.level=warn

# Settings to quiet third party logs that are too verbose
#logger.org.spark-project.jetty=WARN
logger.TaskSetManager.name = org.apache.spark.scheduler.TaskSetManager
logger.TaskSetManager.level = warn

logger.DAGScheduler.name = org.apache.spark.scheduler.DAGScheduler
logger.DAGScheduler.level = warn

logger.Executor.name = org.apache.spark.executor.Executor
logger.Executor.level = warn

logger.NewHadoopRDD.name = org.apache.spark.rdd.NewHadoopRDD
logger.NewHadoopRDD.level = warn

logger.FileOutputCommitter.name = org.apache.hadoop.mapreduce.lib.output.FileOutputCommitter
logger.FileOutputCommitter.level = warn

logger.SparkHadoopMapRedUtil.name = org.apache.spark.mapred.SparkHadoopMapRedUtil
logger.SparkHadoopMapRedUtil.level = warn

#logger.org.spark-project.jetty.util.component.AbstractLifeCycle=ERROR
#logger.org.apache.spark.repl.SparkIMain$exprTyper=INFO
#logger.org.apache.spark.repl.SparkILoop$SparkILoopInterpreter=INFO

logger.parquet.name = org.apache.parquet
logger.parquet.level = error

# SPARK-9183: Settings to avoid annoying messages when looking up nonexistent UDFs in SparkSQL with Hive support
#logger.org.apache.hadoop.hive.metastore.RetryingHMSHandler=FATAL
#logger.org.apache.hadoop.hive.ql.exec.FunctionRegistry=ERROR

# Silence "Initialized Parquet WriteSupport with Catalyst schema"
logger.ParquetWriteSupport.name = org.apache.spark.sql.execution.datasources.parquet.ParquetWriteSupport
logger.ParquetWriteSupport.level = warn

logger.FileScanRDD.name = org.apache.spark.sql.execution.datasources.FileScanRDD
logger.FileScanRDD.level = warn

logger.CodeGenerator.name = org.apache.spark.sql.catalyst.expressions.codegen.CodeGenerator
logger.CodeGenerator.level = warn

logger.ContextHandler.name = org.sparkproject.jetty.server.handler.ContextHandler
logger.ContextHandler.level = warn

logger.SQLHadoopMapReduceCommitProtocol.name = org.apache.spark.sql.execution.datasources.SQLHadoopMapReduceCommitProtocol
logger.SQLHadoopMapReduceCommitProtocol.level = warn

logger.SecurityManager.name = org.apache.spark.SecurityManager
logger.SecurityManager.level = warn

logger.BlockManagerMaster.name = org.apache.spark.storage.BlockManagerMaster
logger.BlockManagerMaster.level = warn

logger.BlockManagerMasterEndpoint.name = org.apache.spark.storage.BlockManagerMasterEndpoint
logger.BlockManagerMasterEndpoint.level = warn

logger.CodecPool.name = org.apache.hadoop.io.compress.CodecPool
logger.CodecPool.level = warn

logger.InMemoryFileIndex.name = org.apache.spark.sql.execution.datasources.InMemoryFileIndex
logger.InMemoryFileIndex.level = warn

# Reading file path:
logger.FilePartitionReader.name = org.apache.spark.sql.execution.datasources.v2.FilePartitionReader
logger.FilePartitionReader.level = warn

logger.SharedState.name = org.apache.spark.sql.internal.SharedState
logger.SharedState.level = warn

logger.ResourceProfile.name = org.apache.spark.resource.ResourceProfile
logger.ResourceProfile.level = warn

logger.ResourceProfileManager.name = org.apache.spark.resource.ResourceProfileManager
logger.ResourceProfileManager.level = warn