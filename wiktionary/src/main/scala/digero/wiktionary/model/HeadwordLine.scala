package digero.wiktionary.model

import digero.wiktionary.model.lexical.{GrammaticalGender, Inflection}

// Represents a single headword line
case class HeadwordLine(text: Either[String, Transliterated],
                        inflections: Seq[Inflection] = Seq.empty,
                        genders: Seq[GrammaticalGender] = Seq.empty,
                        categories: Set[Category] = Set.empty):

  def headword: String = text match
    case Left(string) => string
    case Right(transliterated) => transliterated.text

  def isNonLemma: Boolean = categories.exists(_.name.endsWith("non-lemma forms"))
