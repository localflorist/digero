package digero.wiktionary.model

import org.apache.spark.sql.Row

case class EntryName(name: String, languageCode: String):
  override def toString: String = f"$languageCode:$name"


object EntryName:
  def apply(row: Row): EntryName =
    apply(row.getAs[String]("entry"), row.getAs[String]("language"))
