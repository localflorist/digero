package digero.wiktionary.model

case class TranslationGroup(gloss: String,
                            translations: Seq[Translation])


case class Translation(language: String,
                       translation: String,
                       transliteration: Option[String],
                       script: String,
                       qualifier: Option[String],
                       needsVerifying: Boolean)
