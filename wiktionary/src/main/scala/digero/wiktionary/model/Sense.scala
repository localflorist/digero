package digero.wiktionary.model

enum SenseKind:
  case FormOf

case class Sense(text: String,
                 html: String,
                 kind: Set[SenseKind] = Set.empty,
                 subsenses: Seq[Sense] = Seq.empty):
  
  def isNonLemma: Boolean = kind.contains(SenseKind.FormOf)
  
  override def toString: String =
    def tree(sense: Sense, level: Int): String =
      f"${"    " * level}${sense.text}\n" + sense.subsenses.map(tree(_, level + 1)).mkString("")

    tree(this, 0)