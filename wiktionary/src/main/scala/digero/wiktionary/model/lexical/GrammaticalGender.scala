package digero.wiktionary.model.lexical

import digero.wiktionary.model.Q

// Wikidata: Q162378
enum GrammaticalGender(val shortForm: String,
                       val id: Int,
                       val wikidataItem: Q):
  case Masculine extends GrammaticalGender("m", 0, Q(499327))
  case Feminine extends GrammaticalGender("f", 1, Q(1775415))
  case Neuter extends GrammaticalGender("n", 2, Q(1775461))
  case Common extends GrammaticalGender("c", 3, Q(1305037))
  
  // sometimes treated as grammatical/semantic category of noun
  case Inanimate extends GrammaticalGender("inan", 4, Q(51927539))
  case Animate extends GrammaticalGender("anim", 5, Q(51927507))


object GrammaticalGender:
  def apply(string: String): Option[GrammaticalGender] =
    GrammaticalGender.values.find(_.shortForm == string)
    