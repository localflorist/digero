package digero.wiktionary.model

import digero.wiktionary.model.lexical.Pronunciation
import digero.wiktionary.parser.{HeadwordLineParser, PronunciationParser, SenseParser, TranslationParser, UsageNotesParser}

case class Entry[Content](
     name: EntryName,
     etymologySection: Option[Section[Content]] = None,
     posSections: Seq[Section[Content]] = Seq.empty[Section[Content]],
     remainingSections: Seq[Section[Content]] = Seq.empty[Section[Content]]
):
  def pageTitle: String = name.name

object Entry:
  def apply[T](parsedEntry: ParsedEntry[T]): Entry[T] =
    val (etymologies, posSections, rest) = parsedEntry.sections.split(_.isEtymology)
    assert(etymologies.size <= 1)
    Entry(name=parsedEntry.name,
          etymologySection=etymologies.headOption,
          posSections,
          rest)

extension (entry: Entry[String])
  def pronunciations: Seq[Pronunciation] =
    for
      pronunciationContent <- entry.remainingSections.filter(_.isPronunciation).flatMap(_.content)
      pronunciation <- PronunciationParser().parse(pronunciationContent)
    yield
      pronunciation

  def partsOfSpeech: Seq[PartOfSpeechSection] =
    val senseParser = SenseParser()
    val headwordParser = HeadwordLineParser()

    for
      posSection <- entry.posSections
      posHeader <- posSection.heading
      posContent <- posSection.content
    yield
      PartOfSpeechSection(
        posHeader,
        headwordParser.parse(posContent),
        senseParser.parse(posContent, entry.name),
        translations(posSection),
        usageNotes(posSection),
      )

  private def usageNotes(posSection: Section[String]): Option[String] =
    posSection.subsections
      .filter(_.isUsageNotes)
      .flatMap(_.content)
      .flatMap(UsageNotesParser().parseUsageNotesSection)
      .headOption

  private def translations(posSection: Section[String]): Seq[TranslationGroup] =
    for
      translationContent <- posSection.subsections.filter(_.isTranslations).flatMap(_.content)
      group <- TranslationParser().parseTranslationSection(entry.name, translationContent)
    yield
      group
