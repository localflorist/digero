
package digero.wiktionary.model

object EntryGroup:
  def apply[T](parsedEntry: ParsedEntry[T]): Seq[Entry[T]] =
    parsedEntry.sections match
      case EntryHeaders(headers*) => headers.flatMap(_.build(parsedEntry.name))


extension[T] (entryHeader: EntryHeader[T])
  private def build(name: EntryName): Seq[Entry[T]] =
    visit(name, Seq())

  private def visit(name: EntryName, previous: Seq[EntryHeader[T]]): Seq[Entry[T]] =
    entryHeader.children match
      case Nil =>
        // leaf node
        val sections = entryHeader.allSections ++ previous.flatMap(_.allSections)
        val (etymology, pos, rest) = sections.split(_.isEtymology)

        Seq(Entry(
            name = name,
            etymologySection = etymology.headOption,
            pos,
            rest ++ etymology.drop(1).distinct
        ))
      case children =>
        children.flatMap(c => c.visit(name, previous appended entryHeader))

extension[T] (sections: Seq[Section[T]])
  private def split(p: Section[T] => Boolean): (Seq[Section[T]], Seq[Section[T]], Seq[Section[T]]) =
    val (main, rest) = sections.partition(p)
    val (pos, posRest) = rest.partition(_.isPartOfSpeech)
    (main, pos, posRest)
