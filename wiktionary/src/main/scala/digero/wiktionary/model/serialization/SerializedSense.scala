package digero.wiktionary.model.serialization

import digero.wiktionary.model.Sense

case class SerializedSense(
    definition: String,
    subsenses: Seq[SerializedSense] = Seq.empty
) derives CustomPickler.ReadWriter

object SerializedSense:
  def apply(sense: Sense,
            textConverter: String => Option[String]): SerializedSense =
    SerializedSense(definition = textConverter(sense.html).getOrElse(""),
                    subsenses = sense.subsenses.map(SerializedSense.apply(_, textConverter)))
