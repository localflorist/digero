package digero.wiktionary.model.parsoid

import digero.wiktionary.model.serialization.CustomPickler

type DataTemplateParams = Map[String, DataValue]
type DataTarget = Map[String, String]
type DataAttrib = Either[String, Map[String, String]]

case class DataValue(wt: String) derives CustomPickler.ReadWriter
case class DataTemplate(target: DataTarget, params: DataTemplateParams, i: Int) derives CustomPickler.ReadWriter:
  def name: Option[String] =
    target.get("wt").orElse(target.get("template"))
  
case class DataTemplateContainer(template: DataTemplate) derives CustomPickler.ReadWriter

type Part = Either[String, DataTemplateContainer]
case class ParsoidAttribs(attribs: Seq[Seq[DataAttrib]]) derives CustomPickler.ReadWriter
case class ParsoidTemplate(parts: Seq[Part]) derives CustomPickler.ReadWriter
case class ParsoidError(key: String, message: String) derives CustomPickler.ReadWriter
case class ParsoidErrors(errors: Seq[ParsoidError]) derives CustomPickler.ReadWriter
case class ParsoidCaption(caption: String) derives CustomPickler.ReadWriter
case class ParsoidTemplateStyle(name: String) derives CustomPickler.ReadWriter

type ParsoidData = ParsoidTemplate | ParsoidCaption | ParsoidTemplateStyle | ParsoidAttribs | ParsoidErrors
