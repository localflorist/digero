package digero.wiktionary.model

/**
 * An individual section containing a POS (such as "Noun"),
 * and the corresponding senses.
 *
 * @param section
 * @param headwordLines normally there is only one per POS header
 * @param senses for all headword lines
 */
case class PartOfSpeechSection(section: SectionHeading,
                               headwordLines: Seq[HeadwordLine],
                               senses: Seq[Sense],
                               translations: Seq[TranslationGroup],
                               usageNotes: Option[String]):

  def name: String = section.name
  def isNonLemma: Boolean = headwordLines.exists(_.isNonLemma)

  def categories: Seq[String] = Set(headwordLines.flatMap(_.categories.map(_.name)): _*).toSeq