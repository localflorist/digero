package digero.wiktionary.tasks

import digero.wiktionary.parser.{CommonMarkConverter, EntryParser}
import digero.{DataFrameTask, TaskArguments}
import digero.spark.getOpt
import digero.wiktionary.model.serialization.{CustomPickler, SerializedEntry}
import digero.wiktionary.model.{EntryGroup, EntryName, partsOfSpeech, pronunciations}
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder
import org.apache.spark.sql.types.DataTypes.StringType
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Encoder, Row, SaveMode, SparkSession}

import java.io.File

class ExtractContent extends DataFrameTask[File]:
  private val parser = EntryParser()

  def process(session: SparkSession, input: DataFrame, arguments: TaskArguments): File =
    val output = arguments.outputFile
    val language = arguments.language.getOrElse("en")

    require(output.getPath.nonEmpty)

    given encoder: Encoder[Row] = ExpressionEncoder((new StructType).add("json", StringType))

    (for
      row <- input.filter(s"language = \"$language\"")
      html: String <- row.getOpt("html")
      entryName: EntryName <- Some(EntryName(row))
      entry <- EntryGroup(parser.parse(entryName, html))
      partOfSpeech <- entry.partsOfSpeech
      etymology = entry.etymologySection.flatMap(_.content)
      serializedEntry = SerializedEntry(entry.pageTitle, etymology, entry.pronunciations, partOfSpeech,
                                        textConverter = commonMarkConverter.convert)
    yield Row(CustomPickler.write(serializedEntry)))
      .write
      .mode(SaveMode.Overwrite)
      .text(output.getAbsolutePath)

    output

  private lazy val commonMarkConverter = CommonMarkConverter()