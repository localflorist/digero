package digero.wiktionary

import org.apache.spark.sql.types.DataTypes.{BinaryType, IntegerType, StringType, TimestampType}
import org.apache.spark.sql.types.{ArrayType, StructField, StructType}

object AppSchema:

  // Target schema for parsed XML dumps
  lazy val parsed_xml: StructType = new StructType()
    .add("entry", StringType, nullable = false)     // page title
    .add("namespace", IntegerType, nullable = false)
    .add("language", StringType, nullable = true)   // language code
    .add("parsed", BinaryType, nullable = false)    // contains serialized Sweble parse tree
    .add("last_modified", TimestampType, nullable = true)
    .add("redirect", StringType, nullable = true)   // redirect target

  // Target schema for parsed HTML dumps
  lazy val parsed_html: StructType = StructType(Seq(
    StructField("entry", StringType, nullable=false),      // page title
    StructField("namespace", IntegerType, nullable=false),
    StructField("language", StringType, nullable=false),   // language code
    StructField("html", StringType, nullable=false),
    StructField("redirects", ArrayType(StringType, false), nullable=true),
    StructField("last_modified", TimestampType, nullable = false)
  ))

  // Schema for link graphs
  lazy val links: StructType = StructType(Seq(
    StructField("source", StringType, nullable=false),
    StructField("sourceLanguage", StringType, nullable=false),
    StructField("target", StringType, nullable=false),
    StructField("targetLanguage", StringType, nullable=true)
  ))
