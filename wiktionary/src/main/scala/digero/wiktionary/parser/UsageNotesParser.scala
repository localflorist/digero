package digero.wiktionary.parser;

import digero.wiktionary.parser.JsoupExtensions.*

class UsageNotesParser extends HTMLParser:

  def parseUsageNotesSection(html: String): Option[String] =
    val document = parseDocument(html)
    document
      .select("h4#Usage_notes")
      .map(_.nextElementSibling)
      .filter(_.nodeName() == "p")
      .map(_.text)
      .headOption
