package digero.wiktionary.parser

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.parser.Parser

trait HTMLParser:
  lazy val parser: Parser = Parser.htmlParser

  def parseDocument(html: String): Document = Jsoup.parse(html, parser)
