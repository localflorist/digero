package digero.wiktionary.parser

import digero.wiktionary.model.{EntryName, Sense}
import org.jsoup.nodes.{Element, Node}
import digero.wiktionary.parser.JsoupExtensions.*
import org.jsoup.select.NodeFilter.FilterResult

class SenseParser extends HTMLParser with Logger:
  def parse(html: String, entry: EntryName): Seq[Sense] =
    val document = parseDocument(html)
    val lists = document.select("body > ol")
    lists.size() match
      case 0 => Seq.empty
      case 1 => parseList(lists.first())
      case _ =>
        log.warn(s"more than one list found in entry $entry")
        Seq.empty

  private def parseList(element: Element): Seq[Sense] =
    for
      li <- element.select("> li")
      element = li.clone().filter((node: Node, _) =>
        node match
            case e: Element if Set("dl", "ul", "ol").contains(e.tagName) => FilterResult.REMOVE
            case _ => FilterResult.CONTINUE
      )
      html = element.preprocessed
      text = element.text()

      subList = li.select("> ol").headOption
      kind = CSSClass.applicable(li).flatMap(_.senseKind)
      subsenses = subList.map(parseList).getOrElse(Seq.empty)
    yield Sense(text, html, subsenses=subsenses, kind=kind)
