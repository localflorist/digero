package digero.wiktionary.parser

import digero.model.Template
import digero.wiktionary.model.parsoid.{ParsoidAttribs, ParsoidCaption, ParsoidData, ParsoidErrors, ParsoidTemplate, ParsoidTemplateStyle}
import digero.wiktionary.model.serialization.CustomPickler
import org.jsoup.nodes.Document.OutputSettings
import org.jsoup.nodes.{Document, Element}
import org.jsoup.select.Elements

import scala.collection.WithFilter
import scala.jdk.CollectionConverters.*
import scala.util.Try

object JsoupExtensions:
  extension (elements: Elements)
    def foreach[U](f: Element => U): Unit = elements.asScala.foreach(f)
    def flatMap[B](f: Element => IterableOnce[B]): Seq[B] = elements.asScala.toSeq.flatMap(f)
    def map[B](f: Element => B): Seq[B] = elements.asScala.toSeq.map(f)
    def withFilter(f: Element => Boolean): WithFilter[Element, Seq] = elements.asScala.toSeq.withFilter(f)

    def head: Element = elements.first()
    def headOption: Option[Element] =
      if elements.size() == 0 then None else Some(elements.get(0))

  extension (element: Element)
    def head: Element = element.child(0)
    def headOption: Option[Element] = if element.childrenSize() == 0 then None else Some(element.child(0))
    def attrOpt(key: String): Option[String] = if element.hasAttr(key) then Some(element.attr(key)) else None
    def parsoidData: Option[ParsoidData] =
        attrOpt("data-mw").flatMap { data =>
          Try(CustomPickler.read[ParsoidTemplate](data))
            .orElse(Try(CustomPickler.read[ParsoidCaption](data)))
            .orElse(Try(CustomPickler.read[ParsoidTemplateStyle](data)))
            .orElse(Try(CustomPickler.read[ParsoidAttribs](data)))
            .orElse(Try(CustomPickler.read[ParsoidErrors](data)))
            .toOption
        }

    def templates: Seq[Template] =
      parsoidData match
        case Some(ParsoidTemplate(parts)) =>
          for
            part <- parts
            container <- part.toOption
            template = container.template
            name <- template.name
          yield Template(name)
        case _ => Seq.empty


    def preprocessed: String =
      val copy = element.clone()
      HTMLPreprocessor.removeUnwanted(copy)
      copy.html()

  extension (document: Document)
    def serialize: String =
      document.outputSettings.prettyPrint(false)
      document.outputSettings.syntax(OutputSettings.Syntax.xml)
      document.body.html
