package digero.wiktionary.parser

import digero.wiktionary.model.{EntryName, Translation, TranslationGroup}
import digero.wiktionary.parser.JsoupExtensions.*
import org.jsoup.nodes.Element

class TranslationParser extends HTMLParser with Logger:

  def parseTranslationSection(entry: EntryName, html: String): Seq[TranslationGroup] =
    if entry.languageCode != "en" then
      log.warn(s"translation found in entry $entry")

    val document = parseDocument(html)
    for
      translationTable <- document.select("table.translations")
      gloss = translationTable.attr("data-gloss")
    yield
      TranslationGroup(gloss=gloss, translations = parseTranslations(translationTable))


  private def parseTranslations(table: Element): Seq[Translation] =
    for
      listItem <- table.select("td > ul > li")
      translation <- parseListItem(listItem)
    yield
      translation

  private def parseListItem(listItem: Element): Seq[Translation] =
    for
      spanLang <- listItem.select("span[lang]") if !spanLang.hasClass("tr")
      language = spanLang.attr("lang")
      translation = spanLang.text()
      script = spanLang.className()
      transliteration = spanLang
                          .nextElementSiblings()
                          .select(s"span[lang=${language}-Latn]")
                          .headOption.map(_.text())
      qualifier = spanLang
                    .nextElementSiblings()
                    .select("span.qualifier-content")
                    .headOption.map(_.text())
    yield
      Translation(
          language,
          translation,
          transliteration,
          script,
          qualifier,
          needsVerifying = false
      )
