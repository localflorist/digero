#!/usr/bin/env python
# encoding: utf-8
from pywikibot import Page, Site
import os
os.environ['PYWIKIBOT_USERINTERFACE_LANG'] = 'en'


def get_word_list(page_name: str):
    print('=> getting list of words from %s' % page_name)
    site = Site('en', 'wiktionary')
    page = Page(site, page_name)
    if page.exists():
        return page.text.split('\n')
    else:
        raise Exception('Page %s not found' % page_name)


def upload(page, text, summary, redirect_page=None):
    print('=> uploading page %s (%s)' % (page, summary))
    site = Site('en', 'wiktionary')
    page = Page(site, page)
    page.text = text
    page.save(summary)

    if redirect_page:
        print('=> creating redirect %s -> %s)' % (redirect_page, page))
        r_page = Page(site, redirect_page)
        r_page.set_redirect_target(page, create=True, summary='redirect page')
