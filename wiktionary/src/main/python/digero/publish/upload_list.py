#!/usr/bin/env python
import json
from wiktionary import upload as upload_page
from tables import make_word_table


def upload(path, language, timestamp, max_words):
    with open(path, 'r') as f:
        terms = [json.loads(line) for line in f.readlines()]
        table = make_word_table(terms[0:max_words], language)
        upload_page('User:Jberkel/lists/Tatoeba/%s/%s' % (timestamp, language), table, summary='word list')


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Uploads a word list')
    parser.add_argument('list.jsonl', help='word list')
    parser.add_argument('language', help='language code')
    parser.add_argument('timestamp', help='timestamp')
    parser.add_argument('--max', help='max', type=int, default=1000)

    args = vars(parser.parse_args())

    upload(args['list.jsonl'], args['language'], args['timestamp'], args['max'])
