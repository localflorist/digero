#!/usr/bin/env python
import json
import re
import sys
from dataclasses import dataclass
from functools import total_ordering

from ipanema import query_language
from textwrap import dedent
from functools import reduce


@dataclass
@total_ordering
class LanguageStat:
    code: str
    language_name: str

    entries: int = 0
    entries_forms: int = 0

    definitions: int = 0
    definitions_forms: int = 0

    def total_entries(self):
        return self.entries + self.entries_forms

    def total_definitions(self):
        return self.definitions + self.definitions_forms

    def language_link(self) -> str:
        if self.language_name == "Total":
            return self.language_name

        cat_name = self.language_name if self.language_name.endswith("Language") \
            else self.language_name + " language"
        return f"[[:Category:{cat_name}|{self.language_name}]]"

    def __lt__(self, other):
        return self.total_entries() < other.total_entries()


def calc_total(stats: list[LanguageStat]) -> LanguageStat:
    return reduce(lambda x, y: LanguageStat(
        code="total", language_name="Total",
        entries=x.entries + y.entries,
        entries_forms=x.entries_forms + y.entries_forms,
        definitions=x.definitions + y.definitions,
        definitions_forms=x.definitions_forms + y.definitions_forms
    ), stats)


def summary(stats: list[LanguageStat], current_dump, previous_dump) -> str:
    brackets = [
        range(10_000, sys.maxsize),
        range(1000, 10_000),
        range(100, 1000),
        range(10, 100),
        range(2, 10),
        range(1, 2)
    ]

    counts = [
        len([s for s in stats if s.definitions in bracket]) for bracket in brackets
    ]

    return f"""
'''Warning:''' This information is inexact.{{{{refn|group=note|These statistics are now based on the Wikimedia
 HTML “enterprise” dumps which unfortunately still have data quality issues ([[phab:T305407|T305407]]).
They also don't include Wiktionary-specific namespaces such as Appendix: or
 Reconstruction: ([[phab:T303652|T303652]]).
The statistics are therefore necessarily incomplete and might differ significantly from previous runs.}}}}
It was generated from the {current_dump} dump by [[User:Jberkel]].

At that time, there were '''{len(stats)}''' language headers on Wiktionary, excluding appendix-only languages.

Counting only gloss definitions,{{{{refn|group=note|A gloss definition is one that has definitional content.
A non-gloss definition is one that does not have definitional content but rather links to a gloss definition,
such as inflections, variants and alternative spellings. A gloss entry is an entry that contains at least
one gloss definition.}}}} we have:
* '''{counts[0]}''' languages with 10000 or more definitions
* '''{counts[1]}''' languages with 1000 to 9999 definitions
* '''{counts[2]}''' languages with 100 to 999 definitions
* '''{counts[3]}''' languages with 10 to 99 definitions.
* '''{counts[4]}''' languages with 2 to 9 definitions.
* '''{counts[5]}''' languages with a single definition.

The change columns indicate the change since the {previous_dump} database dump.
"""


def main(file_path: str, old_stats_path: str):
    stats = read_stats(file_path)
    old_stats = read_stats(old_stats_path)

    sorted_list = sorted(stats.values(), key=lambda x: x.language_name)
    pairs = [(row, old_stats.get(row.code, LanguageStat(row.code, language_name=row.language_name)))
             for row in sorted_list]

    totals = [(calc_total([new for new, _ in pairs]),
               LanguageStat(code="total", language_name="Total"))]

    table = make_stats_table(totals + pairs)
    print(summary(sorted_list, current_dump=timestamp(file_path), previous_dump=timestamp(old_stats_path)))
    print(table)
    print("""<references group="note"/>""")


def timestamp(path: str) -> str:
    if match := re.compile(r"/(202\d{5})").search(path):
        group = match.group(1)
        year, month, day = group[0:4], group[4:6], group[6:]
        return f"{year}-{month}-{day}"
    else:
        raise Exception(f"Could not get timestamp from path {path}")


def read_stats(file_path: str) -> dict[str, LanguageStat]:
    with open(file_path, 'r') as file:
        stats = {}
        for row in [json.loads(line) for line in file]:
            language_code = row['language']

            if language_code not in stats:
                if language := query_language(language_code):
                    stats[language_code] = \
                        LanguageStat(code=language_code,
                                     language_name=language.canonical_name)
                else:
                    print(f"missing language code {language_code}", file=sys.stderr)
                    continue

            stat = stats[language_code]
            match row['type'], row['forms']:
                case 'sense', False:
                    stat.definitions = row['count']
                case 'sense', True:
                    stat.definitions_forms = row['count']
                case 'headword', False:
                    stat.entries = row['count']
                case 'headword', True:
                    stat.entries_forms = row['count']
        return stats


def make_stats_table(rows: list[(LanguageStat, LanguageStat)]):
    def format_change(current: int, previous: int) -> str:
        if not previous:
            return ""
        diff = current - previous
        if diff == 0:
            return ""

        color = 'red' if diff < 0 else 'green'
        diff = f"+{diff}" if diff > 0 else diff
        return f"style=\"text-align:left;\" | <span style=\"color:{color}; font-size: 0.8em\">{diff}</span>"

    return dedent("""\
        {| class="sortable prettytable"
         ! language
         ! gloss definitions
         ! <small>change</small>
         ! entries
         ! <small>change</small>
         ! gloss entries
         ! <small>change</small>
         ! form definitions
         ! <small>change</small>
         ! total definitions
         ! <small>change</small>
        %s
         |-
         |}
    """) % '\n'.join(
        dedent(f"""\
|- style="text-align:right;"
{'!' if index == 0 else '|'} style="text-align:left;" | {current.language_link()}\
|| {current.definitions}         || {format_change(current.definitions, old.definitions)}\
|| {current.total_entries()}     || {format_change(current.total_entries(), old.total_entries())}\
|| {current.entries}             || {format_change(current.entries, old.entries)}\
|| {current.definitions_forms}   || {format_change(current.definitions_forms, old.definitions_forms)}\
|| {current.total_definitions()} || {format_change(current.total_definitions(), old.total_definitions())}\
""") for (index, (current, old)) in enumerate(rows))


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Generate Wiktionary stats')
    parser.add_argument('aggregated-stats.jsonl', help='The aggregated stats file')
    parser.add_argument('old-aggregated-stats.jsonl', help='The old stats')
    args = vars(parser.parse_args())

    main(args['aggregated-stats.jsonl'], args['old-aggregated-stats.jsonl'])
