#!/usr/bin/env python
import json
from pathlib import Path
from textwrap import dedent
from pyjavaproperties import Properties
from wiktionary import upload


def main(file_path: Path):
    config = Properties()
    config.load(open(Path(Path(__file__).parent, '..', '..', '..', '..', 'gradle.properties')))
    dump_version = config['dump_version']

    if file_path.is_dir:
        file_path = next(file_path.glob('*.json'))

    with open(file_path, 'r') as file:
        data = [json.loads(line) for line in file]
        table = dedent("""\
                {| class="wikitable sortable"
                |-
                ! Entry
                ! Language
                ! Label
                ! Language
                |-
                %s
                |-
                |}
            """) % '\n'.join("""|-\n| {{l|%s|%s}} \n| %s \n| %s \n| %s""" %
                             (row['entryLanguage'], row['entry'], row['entryLanguage'],
                              row['indicatedLanguage'], row['resolvedLanguage'])
                             for row in data)

        page = f'User:Jberkel/lists/L2-header-label-mismatch/{dump_version}'
        upload(page=page, text=table, summary='mismatched labels')


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Publish misplaced labels.')
    parser.add_argument('misplaced-labels.jsonl', help='The misplaced labels file')
    args = vars(parser.parse_args())

    main(Path(args['misplaced-labels.jsonl']))
