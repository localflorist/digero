#!/usr/bin/env python
import pyspark
from pyspark.sql.functions import col, collect_list, sort_array, count
from pyspark.sql import SparkSession, DataFrame
from publish.wiktionary import get_word_list


# Needs taxonomic data in the form of a TSV file (ColDP archive)
# https://www.catalogueoflife.org/data/download
def read_taxonomy(spark: SparkSession, path):
    print('=> reading taxonomy')
    return spark.read \
        .option('delimiter', '\t') \
        .csv(path, header=True, inferSchema=True) \
        .select(col('col:scientificName').alias('scientificName'))


def read_index(spark, path) -> DataFrame:
    print('=> reading index')
    return spark.read.text(path)


def read_links(spark: SparkSession, path) -> DataFrame:
    print('=> reading links')
    return spark.read.parquet(path) \
        .select('source', 'target') \
        .filter('targetLanguage is null')


def read_excluded_taxa(spark: SparkSession) -> DataFrame:
    excluded = [(x,) for x in get_word_list(page_name='User:Jberkel/lists/wanted/excluded-taxa')]
    return spark.sparkContext.parallelize(excluded).toDF(['word'])


def main(links_path, index_path, taxonomy_path, output_path):
    pyspark.SparkContext.setSystemProperty('org.xerial.snappy.tempdir', 'tmp')

    spark = pyspark.sql.SparkSession.builder \
        .master('local[*]') \
        .getOrCreate()

    excluded = read_excluded_taxa(spark)
    taxonomy = read_taxonomy(spark, taxonomy_path)
    index = read_index(spark, index_path)
    links = read_links(spark, links_path)

    combined = links \
        .join(index, on=links.target == index.value, how='left_anti') \
        .join(excluded, on=links.target == excluded.word, how='left_anti')

    wanted = combined \
        .join(taxonomy, on=combined.target == taxonomy.scientificName, how='left_semi') \
        .groupBy('target') \
        .agg(sort_array(collect_list('source')).alias('sources'),
             count('target').alias('count')) \
        .orderBy(['count', 'target'], ascending=[0, 1]) \
        .persist()

    wanted.show()
    wanted.repartition(1).write.json(output_path, mode='overwrite')

    print('=> output written to %s' % output_path)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Check translingual taxlinks')
    parser.add_argument('links.parquet', help='The links dataset')
    parser.add_argument('index.txt', help='The index file')
    parser.add_argument('NameUsage.tsv', help='The taxonomic database')
    parser.add_argument('wanted-taxa.json', help='Output path')
    args = vars(parser.parse_args())

    main(args['links.parquet'], args['index.txt'], args['NameUsage.tsv'], args['wanted-taxa.json'])
