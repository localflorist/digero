#!/usr/bin/env python
from dataclasses import dataclass
from itertools import chain
from typing import Iterable, List, Optional, Generator, TypeAlias

from dataclasses_json import dataclass_json
from ipanema import query_language
from ipanema.models import Language
from lexi.fixture import Database
from lexi.models import (Headword, Entry as LexiEntry, Etymology, Metadata, Sense as LexiSense,
                         Pronunciation as LexiPronunciation)
from lexi.models.jwktl import PartOfSpeech, PronunciationType

SCHEMA_VERSION = "202011010000"


@dataclass_json
@dataclass
class Sense:
    definition: str
    subsenses: Optional[List["Sense"]] = None


@dataclass_json
@dataclass
class Pronunciation:
    text: str
    qualifier: list[str] = ()


@dataclass_json
@dataclass
class HeadwordLine:
    headword: str
    transliteration: Optional[str] = None
    gender: list[int] = ()
    inflections: list[any] = ()

    def to_data(self) -> dict[str, any]:
        data = {
            'headword': self.headword
        }
        if transliteration := self.transliteration:
            data["transliterations"] = [transliteration]

        if len(self.gender) > 0:
            data["genders"] = self.gender
        return data


@dataclass_json
@dataclass
class Entry:
    pageTitle: str
    headwordLines: list[HeadwordLine]
    partOfSpeech: str
    senses: list[Sense]
    categories: list[str]
    pronunciations: list[Pronunciation]
    etymology: str | None = None
    usageNotes: str | None = None

    def pos(self) -> PartOfSpeech | None:
        return PartOfSpeech.from_string(self.partOfSpeech)


EntryGenerator: TypeAlias = Generator[Entry, None, None]


def make_dictionary(output: str,
                    dump_version: str,
                    schema_version: str,
                    language: str,
                    entries: Iterable[Entry],
                    index: bool = False):
    with Database(':memory:') as db:
        db.create_tables()

        for entry in entries:
            create_entry(entry)

        _insert_metadata(dump_version, schema_version, query_language(language))
        db.pragma('user_version', int(schema_version[:8]))
        if index:
            db.reindex()
        db.vacuum()
        db.export(output)


def create_entry(entry: Entry):
    pos = entry.pos()
    if pos is None:
        return
    # assert pos is not None, f"unknown pos {entry.partOfSpeech} for {entry}"
    if len(entry.headwordLines) == 0:
        print(f"no headword lines in '{entry.pageTitle}'")
        return

    headword = Headword.get_or_none(headword_text=entry.pageTitle)
    if not headword:
        headword = Headword.create(headword_text=entry.pageTitle,
                                   pos=pos)

    for pronunciation in entry.pronunciations:
        if len(pronunciation.qualifier) == 0:
            LexiPronunciation.create(type=PronunciationType.IPA,
                                     pronunciation_text=pronunciation.text,
                                     headword=headword)
        else:
            for qualifier in pronunciation.qualifier:
                LexiPronunciation.create(type=PronunciationType.IPA,
                                         pronunciation_text=pronunciation.text,
                                         note=qualifier,
                                         headword=headword)

    etymology = Etymology.create(etymology_text=etymology) if (etymology := entry.etymology) else None

    usageNotes = usageNotes if (usageNotes := entry.usageNotes) else None

    headword_line = entry.headwordLines[0]

    lexi_entry = LexiEntry.create(headword_text=entry.pageTitle,
                                  pos=pos,
                                  data=headword_line.to_data(),
                                  etymology=etymology,
                                  usage_notes=usageNotes,
                                  headword_id=headword.id)
    senses = [
        {
            'entry': lexi_entry,
            'sense_text': sense.definition
        } for sense in entry.senses
    ]
    LexiSense.insert_many(senses).execute()


def _insert_metadata(dump_version: str, schema_version: str, language: Language):
    metadata = {
        Metadata.DB_SCHEMA_KEY: schema_version,
        Metadata.LANGUAGE: language.code,
        Metadata.LANGUAGE_NAME: language.canonical_name,
        Metadata.DUMP_VERSION: dump_version,
        Metadata.HEADWORDS: Headword.select().count(),
        Metadata.TRANSLATIONS: 0
    }
    for (key, value) in metadata.items():
        Metadata.create(key=key, value=value)


def _load_data(path: str) -> EntryGenerator:
    with open(path, 'r') as fileinput:
        print(f"reading {path}")
        for line in fileinput:
            yield Entry.from_json(line)


def _load_entries(paths: list[str]) -> EntryGenerator:
    return chain(*[_load_data(path) for path in paths])


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Create dictionary')
    parser.add_argument('content.ndjson', help='The dictionary content', nargs='*')
    parser.add_argument('--language', help='The language code', required=True)
    parser.add_argument('--dump-version', help='The dump version', required=True)
    parser.add_argument('--index', help='Index the content', action='store_true')
    parser.add_argument('--out', help='The database output', required=True)

    args = vars(parser.parse_args())
    make_dictionary(args['out'],
                    dump_version=args['dump_version'],
                    schema_version=SCHEMA_VERSION,
                    language=args['language'],
                    entries=_load_entries(args['content.ndjson']),
                    index=args['index'])
