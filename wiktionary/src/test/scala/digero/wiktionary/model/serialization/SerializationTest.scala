package digero.wiktionary.model.serialization

import digero.wiktionary.model.serialization.CustomPickler.write
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*

class SerializationTest:

  @Test
  def testSerializeToJSON(): Unit =
    val entry = SerializedEntry(
      pageTitle = "foo",
      headwordLines = Seq.empty,
      partOfSpeech = "Noun",
      senses = Seq(
        SerializedSense("sense1"),
        SerializedSense("sense2", subsenses = Seq(SerializedSense("subsense 2.1")))
      ),
      etymology = Some("etymology"),
      pronunciations = Seq.empty,
      translations = Seq.empty,
      usageNotes = None,
      categories = Seq.empty
    )
    write(entry, indent=2) shouldEqual
      """{
        |  "pageTitle": "foo",
        |  "headwordLines": [],
        |  "partOfSpeech": "Noun",
        |  "senses": [
        |    {
        |      "definition": "sense1"
        |    },
        |    {
        |      "definition": "sense2",
        |      "subsenses": [
        |        {
        |          "definition": "subsense 2.1"
        |        }
        |      ]
        |    }
        |  ],
        |  "etymology": "etymology",
        |  "pronunciations": [],
        |  "translations": [],
        |  "usageNotes": null,
        |  "categories": []
        |}""".stripMargin

  @Test
  def testSerializeNoneAsNull(): Unit =
    val entry = SerializedEntry(
        pageTitle = "foo",
        partOfSpeech = "Noun",
        headwordLines = Seq.empty,
        senses = Seq.empty,
        etymology = None,
        pronunciations = Seq.empty,
        translations = Seq.empty,
        usageNotes = None,
        categories = Seq.empty)
    write(entry) shouldEqual
      """{"pageTitle":"foo","headwordLines":[],"partOfSpeech":"Noun","senses":[],"etymology":null,"pronunciations":[],"translations":[],"usageNotes":null,"categories":[]}"""
