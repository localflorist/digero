package digero.wiktionary.model

import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.assertThat
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class SectionHeadingTest:
  private lazy val document = Jsoup.parse("""
      <html>
        <section id="anagrams">
            <h3>Anagrams</h3>
            <p><a href="some link">Foo</a></p>
        <section>
      </html>
      """)

  private lazy val a = document.selectFirst("a")

  @Test
  def testElementFilterMatching(): Unit =
    assertThat(SectionHeading.Anagrams.elementFilter(a)).isTrue

  @Test
  def testElementFilterNonMatching(): Unit =
    assertThat(SectionHeading.UsageNotes.elementFilter(a)).isFalse
