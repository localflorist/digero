package digero.wiktionary.model

import digero.wiktionary.model.Conditions.containing
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Condition

class ElementExtensionsTest:
  private lazy val document = Jsoup.parse("""
      <html>
        <section id="anagrams">
            <h3>Anagrams</h3>
            <p><a href="some link">Foo</a></p>
        <section>
      </html>
      """)

    private lazy val a = document.selectFirst("a")

  @Test
  def testSection(): Unit =
      assertThat(a.section).is(containing("Anagrams"))


object Conditions:
  def containing[T](expected: T): Condition[Option[T]] =
      new Condition[Option[T]]:
        override def matches(value: Option[T]): Boolean = value.contains(expected)
