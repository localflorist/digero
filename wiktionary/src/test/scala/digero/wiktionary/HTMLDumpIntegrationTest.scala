package digero.wiktionary

import digero.{ImportHTMLDump, SparkRunner, TaskArguments, runHTMLTask}
import digero.test.Fixture
import org.assertj.core.api.Assertions.assertThat

import java.io.File
import scala.util.Using

trait HTMLDumpIntegrationTest extends Fixture:

  def importHTMLDump(input: File = fixtureFile("enwiktionary-NS0-test-ENTERPRISE-HTML.json.tar.gz")): File =
    val outputPath = temporaryFile("import-html-dump", "parquet")

    runHTMLTask(ImportHTMLDump(),
      input.getAbsolutePath,
      outputPath.getAbsolutePath)

    assertThat(outputPath).exists()
    println(s"imported dump written to ${outputPath}")

    outputPath


  def parseHTMLDump(input: File): File =
    val outputPath = temporaryFile("parsed-html-dump", "parquet")

    Using.resource(SparkRunner("ParseHTMLDumpTest-write", null)) { runner =>
      val dataset = runner.session.read.parquet(input.getAbsolutePath)
      val arguments = TaskArguments(outputPath.getAbsolutePath)
      ParseHTMLDump().process(runner.session, dataset, arguments)
    }
    assert(outputPath.exists())
    println(s"parsed dump written to ${outputPath}")

    outputPath
    