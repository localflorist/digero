package digero.wiktionary

import digero.TaskArguments
import digero.model.RawPage
import digero.model.RawPage.CONTENT_MODEL_WIKITEXT
import digero.wikitext.SerializationSupport
import digero.wiktionary.model.Namespaces.{Appendix, Main, Reconstruction}
import ipanema.links.Namespace
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.{BeforeEach, Tag}
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.{Arguments, MethodSource}

import java.io.File
import java.time.OffsetDateTime
import scala.util.Using
import scala.jdk.CollectionConverters.*

@Tag("integration")
class ParseXMLDumpTest:
  private var testConf: SparkConf = _

  @BeforeEach def setUp(): Unit =
    testConf = new SparkConf(false)
      .setMaster("local")
      .set("spark.kryo.registrator", classOf[SerializationSupport].getName)
      .set("spark.sql.datetime.java8API.enabled", "true")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.sql.shuffle.partitions", "1")
      .setAppName(getClass.getCanonicalName)


  @ParameterizedTest(name = "testProcessPage({0})")
  @MethodSource(Array("makePages"))
  def testCreate(page: RawPage, expectedResult: Seq[Row]): Unit =
      val subject = ParseXMLDump()

      Using.resource(SparkSession.builder().config(testConf).getOrCreate()) { session =>
        val input = session.sparkContext.parallelize(Seq(page.title -> page))
        val result = subject.process(session.sparkContext, input, TaskArguments(Array("test-parse-dump")))
        assertThat(result).isDirectory

        if expectedResult == null || expectedResult.isEmpty then
          assertThat(result.list).containsExactlyInAnyOrder("_SUCCESS", "._SUCCESS.crc")
        else
          val resultDataset: DataFrame = session.read.parquet(result.getAbsolutePath)
            .select("language", "entry", "redirect")
          val rows = resultDataset.collect()
          assertThat(rows.toSeq.asJava).containsExactlyInAnyOrderElementsOf(expectedResult.asJava)
        ()
      }


object ParseXMLDumpTest:
  def makePages(): Array[Arguments] = Array(
    arguments(makePage(null, null, Main), Seq.empty), // null title
    arguments(makePage("page", "some text", Main), Seq.empty),
    arguments(makePage("page", "==English==\n\n==German==\n\n", Main),
        Seq(Row("en", "page", null),
            Row("de", "page", null))),
    arguments(makePage("Appendix:Lojban/kulnu", "==Lojban==\n\n", Appendix), Seq(Row(null, "Appendix:Lojban/kulnu", null))),
    arguments(makePage("Reconstruction:Proto-Germanic/slangô", "==Proto-Germanic==\n\n", Reconstruction), Seq(Row(null, "Reconstruction:Proto-Germanic/slangô", null))),
    arguments(makePage("Reconstruction:Proto-Germanic/slangô", "#REDIRECT [[foo]]", Reconstruction, "foo"), Seq(Row(null, "Reconstruction:Proto-Germanic/slangô", "foo"))),
    arguments(makePage("page", "#REDIRECT [[foo]]", Main, "foo"), Seq(Row(null, "page", "foo")))
  )

  private def makePage(title: String, text: String, namespace: Namespace, redirect: String = null): RawPage =
    val page = RawPage()
    page.title = title
    page.text = text
    page.namespace = namespace.getId
    page.contentModel = CONTENT_MODEL_WIKITEXT
    page.redirect = redirect
    page.timestamp = OffsetDateTime.now()
    page
