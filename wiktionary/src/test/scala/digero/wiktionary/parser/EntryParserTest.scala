package digero.wiktionary.parser

import digero.wiktionary.model.Level.{L3, L4, L5, L6}
import org.assertj.core.api.Assertions.{assertThat, fail}
import org.junit.jupiter.api.{BeforeEach, Test}

import scala.jdk.CollectionConverters.*
import digero.wiktionary.model.{Entry, EntryName, HeadwordLine, Level, Section, SectionHeading, partsOfSpeech}
import digero.wiktionary.model.SectionHeading.{Adjective, AlternativeForms, Anagrams, Antonyms, Declension, DerivedTerms, Descendants, Etymology, FurtherReading, GlyphOrigin, Noun, Preposition, Pronunciation, ProperNoun, References, RelatedTerms, SeeAlso, Synonyms, Translations, Verb}
import digero.test.Fixture

class EntryParserTest extends Fixture:
  var subject: EntryParser = _

  @BeforeEach
  def prepare(): Unit = subject = EntryParser()

  @Test
  def testParseSingleEtymology(): Unit =
    val result = subject.parse(EntryName("simple", "en"), fixture("simple_en_expected.html"))
    assertThat(result.name).isEqualTo(EntryName("simple", "en"))

    result.sections match
      case Section("Etymology", L3, Some(Etymology), None, _, Nil) ::
           Section("Pronunciation", L3, Some(Pronunciation), None, _, Nil) ::
           Section("Adjective", L3, Some(Adjective), None, _,
              Section("Synonyms", L4, Some(Synonyms), None, _, Nil) ::
              Section("Antonyms", L4, Some(Antonyms), None, _, Nil) ::
              Section("Derived terms", L4, Some(DerivedTerms), None, _, Nil) ::
              Section("Translations", L4, Some(Translations), None, _, Nil) :: Nil
           ) ::
           Section("Noun", L3, Some(Noun), None, _,
              Section("Translations", L4, Some(Translations), None, _, Nil) :: Nil
           ) ::
           Section("Verb", L3, Some(Verb), None, _,
              Section("Derived terms", L4, Some(DerivedTerms), None, _, Nil) :: Nil
           ) ::
           Section("Anagrams", L3, Some(Anagrams), None, _, Nil) :: Nil =>

  @Test
  def testParseMultipleEtymologies(): Unit =
    val result = subject.parse(EntryName("bar", "en"), fixture("bar_en_expected.html"))
    assertThat(result.name).isEqualTo(EntryName("bar", "en"))

    result.sections match
      case Section("Pronunciation", L3, Some(Pronunciation), None, _, Nil) ::
           Section("Etymology 1", L3, Some(Etymology), Some(1), _,
              Section(_, L4, Some(Noun), None, _,
                Section(_, L5, Some(DerivedTerms), None, _, Nil) ::
                Section(_, L5, Some(Descendants), None, _, Nil) ::
                Section(_, L5, Some(Translations), None, _, Nil) :: Nil
              ) ::
              Section(_, L4, Some(SeeAlso), None, _, Nil) ::
              Section(_, L4, Some(References), None, _, Nil) :: Nil
           ) ::
           Section("Etymology 2", L3, Some(Etymology), Some(2), _,
              Section(_, L4, Some(Verb), None, _,
                Section(_, L5, Some(Synonyms), None, _, Nil) ::
                Section(_, L5, Some(DerivedTerms), None, _, Nil) ::
                Section(_, L5, Some(Descendants), None, _, Nil) ::
                Section(_, L5, Some(Translations), None, _, Nil) :: Nil
              ) ::
              Section(_, L4, Some(Preposition), None, _,
                Section(_, L5, Some(Synonyms), None, _, Nil) ::
                Section(_, L5, Some(DerivedTerms), None, _, Nil) ::
                Section(_, L5, Some(Translations), None, _, Nil) :: Nil
              ) ::
              Section(_, L4, Some(References), None, _, Nil) :: Nil
           ) ::
           Section("Etymology 3", L3, Some(Etymology), Some(3), _,
              Section(_, L4, Some(Noun), None, _,
                Section(_, L5, Some(Synonyms), None, _, Nil) ::
                Section(_, L5, Some(DerivedTerms), None, _, Nil) ::
                Section(_, L5, Some(Descendants), None, _, Nil) ::
                Section(_, L5, Some(RelatedTerms), None, _, Nil) ::
                Section(_, L5, Some(Translations), None, _, Nil) :: Nil
              ) :: Nil
           ) ::
           Section("Further reading", L3, Some(FurtherReading), None, _, Nil) ::
           Section("Anagrams", L3, Some(Anagrams), None, _, Nil) ::
           Nil =>

  @Test
  def testParseNestedEtymologies(): Unit =
    val entry = subject.parse(EntryName("lupia", "la"), fixture("la-Lupia.html"))
    entry.sections match
      case Section("Etymology 1", L3, Some(Etymology), Some(1), _,
              Section(_, L4, Some(AlternativeForms), None, _, _) ::
              Section(_, L4, Some(Etymology), None, _, _) ::    // invalid nesting of Etymology
              Section(_, L4, Some(ProperNoun), None, _, _) ::
              Section(_, L4, Some(References), None, _, _) :: Nil
        ) ::
           Section("Etymology 2", L3, Some(Etymology), Some(2), _,
              Section(_, L4, Some(Pronunciation), None, _, _) ::
              Section(_, L4, Some(ProperNoun), None, _, _) ::
              Section(_, L4, Some(Declension), None, _, _) :: Nil // should be nested inside POS
           ) :: Nil =>

  @Test
  def testParseNestedGlyphOrigin(): Unit =
    val entry = subject.parse(EntryName("白露", "ja"), fixture("ja-白露.html"))
    entry.sections match
      case Section("Etymology 1", L3, Some(Etymology), Some(1), _,
          Section(_, L4, Some(Pronunciation), None, _, _) ::
          Section(_, L4, Some(ProperNoun), None, _, _) ::
          Section(_, L4, Some(SeeAlso), None, _, _) :: Nil
      ) ::
        Section("Etymology 2", L3, Some(Etymology), Some(2), _,
          Section(_, L4, Some(Pronunciation), None, _, _) ::
          Section(_, L4, Some(Noun), None, _, _) :: Nil
      ) ::
        Section("Etymology 3", L3, Some(Etymology), Some(3), _,
          Section(_, L4, Some(Pronunciation), None, _, _) ::
          Section(_, L4, Some(ProperNoun), None, _, _) :: Nil
      ) ::
        Section("Etymology 4", L3, Some(Etymology), Some(4), _,
        Section(_, L4, Some(GlyphOrigin), None, _, _) :: // invalid nesting of Etymology
        Section(_, L4, Some(Pronunciation), None, _, _) ::
        Section(_, L4, Some(ProperNoun), None, _, _) :: Nil
      ) :: Nil =>

  @Test
  def testParsePartsOfSpeech(): Unit =
    val entry = Entry(subject.parse(EntryName("simple", "en"), fixture("simple_en_expected.html")))
    assertThat(entry.partsOfSpeech.size).isEqualTo(3)

  @Test
  def testParseSenses(): Unit =
    val entry = Entry(subject.parse(EntryName("simple", "en"), fixture("simple_en_expected.html")))
    assertThat(entry.partsOfSpeech.foldLeft(0)((m, o) =>  m + o.senses.size)).isEqualTo(16)

  @Test
  def testParseHeadwords(): Unit =
    val entry = Entry(subject.parse(EntryName("simple", "en"), fixture("simple_en_expected.html")))
    val headwords = for
      pos <- entry.partsOfSpeech
      headword <- pos.headwordLines
    yield headword

    headwords match
      case HeadwordLine(Left("simple"), _, _, _)
        :: HeadwordLine(Left("simple"), _, _, _)
        :: HeadwordLine(Left("simple"), _, _, _) :: Nil =>
      case e => fail(s"unmatched $e")

  @Test
  def testParseNonLemma(): Unit =
    val entry = Entry(subject.parse(EntryName("played", "en"), fixture("played.html")))
    entry.partsOfSpeech.flatMap(_.headwordLines) match
      case x :: Nil if x.isNonLemma =>
      case e => fail(s"unmatched $e")
