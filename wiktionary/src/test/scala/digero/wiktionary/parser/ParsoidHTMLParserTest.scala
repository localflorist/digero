package digero.wiktionary.parser

import digero.test.Fixture
import org.junit.jupiter.api.{BeforeEach, Test}
import org.assertj.core.api.Assertions.assertThat
import org.jsoup.nodes.Element
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.function.Executable

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters.*

class ParsoidHTMLParserTest extends Fixture:
  var parser: ParsoidHTMLParser = _
  given entry: String = "test"

  @BeforeEach
  def prepare(): Unit = parser = ParsoidHTMLParser()

  @Test
  def testSplitBar(): Unit =
    val entries = parser.split(fixture("bar.html"))
    assertThat(entries.size).isEqualTo(44)
    val en = entries.find(_.language == "English").get

//    fixtureWrite(en.html, "bar_en_expected.html")
    assertThat(en.html).isEqualTo(fixture("bar_en_expected.html"))

  @Test
    def testSplitSimple(): Unit =
    val entries = parser.split(fixture("simple.html"))
    assertThat(entries.size).isEqualTo(17)
    val en = entries.find(_.language == "English").get

//    fixtureWrite(en.html, "simple_en_expected.html")
    assertThat(en.html).isEqualTo(fixture("simple_en_expected.html"))

  @Test
  def testLinks(): Unit =
    val links = parser.links(fixture("bar_en.html"))
    assertThat(links.size).isEqualTo(432)

  @Test
  def testFilteredLinks(): Unit =
    val links = parser.links(fixture("bar_en.html"), _.text.startsWith("a"))
    assertThat(links.size).isEqualTo(12)

  @Test
  def shouldParseLanguageInH2Header(): Unit =
    val entries = parser.split(fixture("basic.html"))
    assertThat(entries.size).isEqualTo(1)
    assertThat(entries.head.language).isEqualTo("Irish")
    assertThat(entries.head.code).isEqualTo(Some("ga"))

  @Test
  def shouldReturnsNoneForUnknownLanguages(): Unit =
    val entries = parser.split(fixture("unknown_language.html"))
    assertThat(entries.size).isEqualTo(2)

    assertThat(entries(0).code).isEqualTo(None)
    assertThat(entries(0).language).isEqualTo("Unknown")
    assertThat(entries(1).code).isEqualTo(Some("en"))
    assertThat(entries(1).language).isEqualTo("English")
