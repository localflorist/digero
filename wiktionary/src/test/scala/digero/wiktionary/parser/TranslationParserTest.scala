package digero.wiktionary.parser

import digero.test.Fixture
import org.scalatest.matchers.should.Matchers.*
import org.junit.jupiter.api.{BeforeEach, Test}
import digero.wiktionary.model.{EntryName, Translation, TranslationGroup}

class TranslationParserTest extends Fixture:
  var subject: TranslationParser = _

  @BeforeEach
  def prepare(): Unit = subject = TranslationParser()

  @Test
  def testParseTranslationSection(): Unit =
    val translations = subject.parseTranslationSection(EntryName("test", "en"),
                                                       fixture("simple_translations_section.html"))
    translations match
      case TranslationGroup(gloss1, translations1) ::
           TranslationGroup(gloss2, translations2) :: Nil =>

          gloss1 shouldEqual "uncomplicated"
          gloss2 shouldEqual "simple-minded"

          translations1 should have length 174
          translations2 should have length 31

          translations1.take(3) match
            case Translation("af", "eenvoudig", None, "Latn", _, false) ::
                 Translation("sq", "thjeshtë", None, "Latn", _, false) ::
                 Translation("ar", "بَسِيط", Some("basīṭ"), "Arab", _, false) :: Nil =>

          translations1.reverse.take(3) match
            case Translation("zza", "gerges", None, "Latn", _, false) ::
                 Translation("yi", "פּשוט", Some("poshet"), "Hebr", _, false) ::
                 Translation("fy", "ienfâldich", None, "Latn", _, false) :: Nil =>

          translations2.take(3) match
            case Translation("hy", "պարզամիտ", Some("parzamit"), "Armn", _, false) ::
                 Translation("hy", "միամիտ", Some("miamit"), "Armn", _, false) ::
                 Translation("bg", "наивен", Some("naiven"), "Cyrl", _, false) :: Nil =>

          translations2.reverse.take(3) match
            case Translation("sv", "enkel", None, "Latn", _, false) ::
                 Translation("sh", "prostodušan", None, "Latn", _, false) ::
                 Translation("sh", "простодушан", None, "Cyrl", _, false) :: Nil =>
