package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.{EntryName, Sense, SenseKind}
import org.assertj.core.api.Assertions.{assertThat, fail}
import org.junit.jupiter.api.{BeforeEach, Test}

class SenseParserTest extends Fixture:
  var subject: SenseParser = _

  @BeforeEach
  def prepare(): Unit = subject = SenseParser()

  @Test
  def testParseFormOf(): Unit =
      subject.parse(fixture("played_senses.html"), EntryName("played", "en")).head match
        case Sense("simple past tense and past participle of play", _, kind, Seq()) if kind.contains(SenseKind.FormOf) =>

  @Test
  def testParse(): Unit =
    val result = subject.parse(fixture("simple_senses.html"), EntryName("simple", "en"))
    assertThat(result.size).isEqualTo(8)
    result.zipWithIndex.foreach((sense, index) => print(f"${index + 1} $sense"))

    result match
      case Sense("Uncomplicated; taken by itself, with nothing added.", _, _, Seq()) ::
           Sense("Without ornamentation; plain.", _, _, Seq()) ::
           Sense("Free from duplicity; guileless, innocent, straightforward.", _, _, Seq()) ::
           Sense("Undistinguished in social condition; of no special rank.", _, _, Seq()) ::
           Sense("(now rare ) Trivial; insignificant.", _, _, Seq()) ::
           Sense("(now colloquial , euphemistic ) Feeble-minded; foolish.", _, _, Seq()) ::
           Sense("(heading, technical) Structurally uncomplicated.", _, _,
              Sense("(chemistry , pharmacology ) Consisting of one single substance; uncompounded.", _, _, Seq()) ::
              Sense("(mathematics ) Of a group: having no normal subgroup.", _, _, Seq()) ::
              Sense("(botany ) Not compound, but possibly lobed.", _, _, Seq()) ::
              Sense("(of a steam engine) Using steam only once in its cylinders, in contrast to a compound engine, where steam is used more than once in high-pressure and low-pressure cylinders.", _, _, Seq()) ::
              Sense("(zoology ) Consisting of a single individual or zooid; not compound.", _, _, Seq()) ::
              Sense("(mineralogy ) Homogenous.", _, _, Seq()) :: Nil)
           ::
           Sense("(obsolete ) Mere; not other than; being only.", _, _, Seq())
           :: Nil =>

      case e => fail(s"not matched $e")
