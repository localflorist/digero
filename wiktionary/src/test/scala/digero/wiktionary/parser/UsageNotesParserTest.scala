package digero.wiktionary.parser

import digero.test.Fixture
import org.scalatest.matchers.should.Matchers.*
import org.junit.jupiter.api.{BeforeEach, Test}
import digero.wiktionary.model.{EntryName}

class UsageNotesParserTest extends Fixture:
  var subject: UsageNotesParser = _

  @BeforeEach
  def prepare(): Unit = subject = UsageNotesParser()

  @Test
  def testPresentUsageNotes(): Unit =
    val usageNotes = subject.parseUsageNotesSection(fixture("simple_usagenotes_section.html"))
    usageNotes should not be empty
    usageNotes.get should startWith ("The second and third meanings")

  @Test
  def testNoUsageNotes(): Unit =
    val usageNotes = subject.parseUsageNotesSection(fixture("water.html"))
    usageNotes shouldBe empty
