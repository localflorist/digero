package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.lexical.InflectionType.{GenitiveSingular, Plural}
import digero.wiktionary.model.lexical.{GrammaticalGender, Inflection, InflectionType}
import digero.wiktionary.model.{Category, HeadwordLine, Transliterated}
import org.assertj.core.api.Assertions.fail
import org.junit.jupiter.api.{BeforeEach, Test}
import org.scalatest.matchers.should.Matchers.*

class HeadwordLineParserTest extends Fixture:
  var subject: HeadwordLineParser = _

  given ordering: Ordering[Category] = (x: Category, y: Category) => x.name.compare(y.name)

  @BeforeEach
  def prepare(): Unit = subject = HeadwordLineParser()

  @Test
  def testParse(): Unit =
    subject.parse(fixture("played_headword.html")) match
      case HeadwordLine(Left("played"), _, _, categories) :: Nil =>
          categories.toSeq.sorted match
            case Category("English non-lemma forms")
              :: Category("English verb forms") :: Nil =>
            case e => fail(s"unmatched ${e}")
      case e => fail(s"unmatched: ${e}")

  @Test
  def testParseGerman(): Unit =
    subject.parse(fixture("haltest_ab_headword.html")) match
      case HeadwordLine(Left("haltest ab"), _, _, categories) :: Nil =>
        categories.toSeq.sorted match
          case Category("German non-lemma forms")
            :: Category("German verb forms") :: Nil =>
          case e => fail(s"unmatched ${e}")
      case e => fail(s"unmatched: ${e}")

  @Test
  def testParseRussianTransliteration(): Unit =
    subject.parse(fixture("ru-noun.html")) match
      case HeadwordLine(Right(Transliterated("мир", "Cyrl", "mir" :: Nil)), _, _, _) :: Nil =>

  @Test
  def testParseKoreanTransliteration(): Unit =
  subject.parse(fixture("ko-noun.html")) match
    case HeadwordLine(Right(Transliterated("야자수", "Kore", "yajasu" :: Nil)), _, _, _) :: Nil =>

  @Test
  def testParseGenderRussian(): Unit =
    subject.parse(fixture("ru-noun.html")) match
      case HeadwordLine(_, _, gender, _) :: Nil =>
        gender should contain only(GrammaticalGender.Masculine, GrammaticalGender.Inanimate)

  @Test
  def testParseGenderRussianAnimate(): Unit =
    subject.parse(fixture("ru-noun-animate.html")) match
      case HeadwordLine(_, _, gender, _) :: Nil =>
        gender should contain only(GrammaticalGender.Feminine, GrammaticalGender.Animate)

  @Test
  def testParseGermanGender(): Unit =
    subject.parse(fixture("de-noun.html")) match
      case HeadwordLine(_, _, gender, _) :: Nil =>
        // headword, diminutive1, diminutive2
        gender shouldEqual Seq(GrammaticalGender.Neuter, GrammaticalGender.Neuter, GrammaticalGender.Neuter)

  @Test
  def testParseGermanMultipleGenders():Unit =
    subject.parse(fixture("de-noun-multiple-genders.html")) match
      case HeadwordLine(_, _, gender, _) :: Nil =>
        gender should contain only (GrammaticalGender.Feminine, GrammaticalGender.Masculine)

  @Test
  def testParseSwedishCommonGender(): Unit =
    subject.parse(fixture("sv-noun.html")) match
      case HeadwordLine(_, _, gender, _) :: Nil =>
        gender should contain only GrammaticalGender.Common

  @Test
  def testParseLatinGender(): Unit =
    subject.parse(fixture("la-noun.html")) match
      case HeadwordLine(_, _, gender, _) :: Nil =>
        gender should contain only GrammaticalGender.Feminine

  @Test
  def testParseLatinHeadword(): Unit =
    subject.parse(fixture("la-noun.html")) match
      case HeadwordLine(Left("pīla"), _, _, _) :: Nil =>

  @Test
  def testParseCatalanPlural(): Unit =
    subject.parse(fixture("ca-noun.html")) match
      case HeadwordLine(_, inflections, _, _) :: Nil =>
        inflections should contain(Inflection(Plural, Seq("mesos")))

  @Test
  def testParseEnglishMultiplePlurals(): Unit =
    subject.parse(fixture("en-noun-multiple-plurals.html")) match
      case HeadwordLine(_, inflections, _, _) :: Nil =>
        inflections should contain(Inflection(Plural, Seq("albatross", "albatrosses")))

  @Test
  def testParseGermanGenitive(): Unit =
    subject.parse(fixture("de-noun.html")) match
      case HeadwordLine(_, inflections, _, _) :: Nil =>
        inflections should contain(Inflection(GenitiveSingular, Seq("Blattes", "Blatts")))

  @Test
  def testParseMultipleHeadwordLines(): Unit =
    subject.parse(fixture("multiple_headword_lines.html")) match
      case HeadwordLine(_, _, genders1, categories1) ::
           HeadwordLine(_, _, genders2, categories2) :: Nil =>
            genders1 should contain only GrammaticalGender.Masculine
            genders2 should contain only GrammaticalGender.Feminine

            categories1 should contain only  (Category("German lemmas"),
                                              Category("German nouns"),
                                              Category("German masculine nouns"))

            categories2 should contain only  (Category("German lemmas"),
                                              Category("German nouns"),
                                              Category("German feminine nouns"))
