package digero.wiktionary.parser

import digero.test.Fixture
import digero.wiktionary.model.{EntryName, ParsedEntry}


trait FixtureParsing extends Fixture:
  def parse(name: EntryName, fixturePath: String): ParsedEntry[String] =
    EntryParser().parse(name, html = fixture(fixturePath))
