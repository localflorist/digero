### Parse dumps

* [ImportHTMLDump](../core/src/main/scala/digero/ImportHTMLDump.scala) → [ParseHTMLDump](src/main/scala/digero/wiktionary/ParseHTMLDump.scala)
* [ImportXMLDump](../core/src/main/scala/digero/ImportXMLDump.scala) → [ParseXMLDump](src/main/scala/digero/wiktionary/ParseXMLDump.scala)

Database dumps are downloaded automatically based on the version defined in
[gradle.properties](/gradle.properties) (`dump_version`).

```sh
$ ./gradlew downloadXMLDump
$ ./gradlew downloadHTMLDump
$ make wiktionary/parsed/20230401/dump-html.parquet
$ make wiktionary/parsed/20230401/dump-xml.parquet
```

### Generate Wiktionary stats

* [ExtractStats](src/main/scala/digero/wiktionary/tasks/stats/ExtractStats.scala)
* [AggregateStats](src/main/scala/digero/wiktionary/tasks/stats/AggregateStats.scala)

```
$ make wiktionary/parsed/20230401/aggregated_stats.ndjson
$ ./wiktionary/src/main/python/digero/publish/publish_stats.py \
    wiktionary/parsed/20230401/aggregated_stats.ndjson/*json \
    wiktionary/parsed/20230301/aggregated_stats.ndjson/*json
```

Specify the same directory twice if you don't have old data, the change
columns will be empty in that case.

### Find wanted entries

* [FindWantedEntries](src/main/scala/digero/wiktionary/tasks/wanted/FindWantedEntries.scala)

### Use on [toolforge]

Important: toolforge jobs currently have a default upper memory limit of 4GB. Java
jobs need to be started with a max heap size set much lower than that (to accommodate 
the OS). On toolforge, the configuration is overridden in `~/.gradle/gradle.properties`.

#### Install Python virtual env

```sh
$ git clone https://gitlab.com/jberkel/digero.git git
$ cd git
$ ./submit python venv make venv
```

`submit` is a small wrapper for `toolforge-jobs`, see [Jobs framework][] for more
details. 

#### Run tasks

```sh
$ ./submit java job-id make wanted_entries
$ ./submit java job-id make aggregate_wanted_entries
$ ./submit java job-id make publish_wanted_entries
```

Set up [user-config.py][] to authenticate with pywikibot.

Process output is written to `~/job-id.[out|err]`.

### Query

Aggregated data can be comfortably queried using [pyspark][] or [pandas][]
inside [jupyter][], check out the [sample notebooks](src/main/jupyter/):

```sh
$ pip install -r wiktionary/requirements.txt
$ jupyter notebook wiktionary/src/main/jupyter
```

You can also use [duckdb][] to directly query the generated parquet files.

```sh
$ duckdb
D select * from read_parquet('wiktionary/parsed/20230601/dump-html-raw.parquet/*.parquet') limit 2;
┌────────┬──────────────────────┬───────────┬───┬──────────────────────┬──────────────────────┬────────────────┐
│   id   │        title         │ namespace │ … │       content        │     content_hash     │ content_length │
│ int64  │       varchar        │   int32   │   │       varchar        │       varchar        │     int32      │
├────────┼──────────────────────┼───────────┼───┼──────────────────────┼──────────────────────┼────────────────┤
│ 273595 │ black-throated diver │         0 │ … │ <!DOCTYPE html>\n<…  │ d32731df99516e5322…  │          23575 │
│ 280515 │ Audrey               │         0 │ … │ <!DOCTYPE html>\n<…  │ b0550d772d487333d1…  │          29735 │
├────────┴──────────────────────┴───────────┴───┴──────────────────────┴──────────────────────┴────────────────┤
│ 2 rows                                                                                   8 columns (6 shown) │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```


[pyspark]: https://spark.apache.org/docs/latest/api/python/pyspark.html
[pandas]: https://pandas.pydata.org/
[jupyter]: https://jupyter.org/
[toolforge]: https://tools.wmflabs.org/
[user-config.py]: https://www.mediawiki.org/wiki/Manual:Pywikibot/user-config.py
[Jobs framework]: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Jobs_framework
[duckdb]: https://duckdb.org/