include gradle.properties

VENV=venv
PIP=$(VENV)/bin/pip
PYTHON=$(VENV)/bin/python
GRADLE=./gradlew
GRADLE_ARGS=

# raw dumps
DUMPS_DIR          = wiktionary/dumps
XML_DUMP_INDEX_BZ2 = $(DUMPS_DIR)/enwiktionary-$(dump_version)-pages-articles-multistream-index.txt.bz2
XML_DUMP_BZ2 	   = $(DUMPS_DIR)/enwiktionary-$(dump_version)-pages-articles-multistream.xml.bz2
HTML_DUMP_TGZ      = $(DUMPS_DIR)/enwiktionary-NS0-$(dump_version)-ENTERPRISE-HTML.json.tar.gz

BASE_DIR         = wiktionary/parsed/$(dump_version)
HTML_DUMP        = $(BASE_DIR)/dump-html.parquet
HTML_DUMP_RAW    = $(BASE_DIR)/dump-html-raw.parquet
HTML_DUMP_CLEAN  = $(BASE_DIR)/dump-html-clean.parquet
XML_DUMP         = $(BASE_DIR)/dump-xml.parquet
XML_DUMP_RAW     = $(BASE_DIR)/dump-xml-raw.parquet
LINKS 	         = $(BASE_DIR)/links.parquet
WANTED_ENTRIES   = $(BASE_DIR)/wanted-entries.parquet
AGGREGATED_DIR 	 = $(BASE_DIR)/aggregated
MISPLACED_LABELS = $(BASE_DIR)/misplaced-labels.jsonl

INDEX_URL 	   = https://gitlab.com/api/v4/projects/22495606/packages/pypi/simple
PYTHON_DIR 	   = wiktionary/src/main/python/digero

$(VENV):
	python3 -m venv $(VENV) --copies
	$(PIP) install --upgrade pip wheel setuptools
	$(PIP) install -r wiktionary/requirements.txt -r wiktionary/requirements-dev.txt --extra-index-url=$(INDEX_URL)

$(PYTHON): $(VENV)

parse: $(HTML_DUMP)
links: $(LINKS)
wanted_entries: $(WANTED_ENTRIES)

$(HTML_DUMP): $(HTML_DUMP_CLEAN)
	$(GRADLE) processDataFrame -Ptask=wiktionary.ParseHTMLDump -PtaskArgs="$< $@" $(GRADLE_ARGS)

$(HTML_DUMP_CLEAN): $(HTML_DUMP_RAW)
	$(GRADLE) processDataFrame -Ptask=CleanHTMLDump -PtaskArgs="$< $@" $(GRADLE_ARGS)

$(HTML_DUMP_RAW):
	$(GRADLE) processCompressedHTMLDump -Ptask=ImportHTMLDump -PtaskArgs=$@ $(GRADLE_ARGS)

$(LINKS): $(HTML_DUMP)
	$(GRADLE) processDataFrame -Ptask=wiktionary.tasks.wanted.CreateLinkGraph -PtaskArgs="$< $@" $(GRADLE_ARGS)

$(WANTED_ENTRIES): $(HTML_DUMP) $(LINKS)
	$(GRADLE) processDataFrame -Ptask=wiktionary.tasks.wanted.FindWantedEntries -PtaskArgs="$^ $@" $(GRADLE_ARGS)

aggregate_wanted_entries: $(AGGREGATED_DIR)
$(AGGREGATED_DIR): $(WANTED_ENTRIES)
	$(GRADLE) processDataFrame -Ptask=wiktionary.tasks.wanted.AggregateWantedEntries -PtaskArgs="$^ $@" $(GRADLE_ARGS)

publish_wanted_entries: $(PYTHON) user-config.py $(AGGREGATED_DIR)
	$(PYTHON) $(PYTHON_DIR)/publish/publish_wanted_entries.py $(AGGREGATED_DIR)

$(XML_DUMP_BZ2):
	$(GRADLE) wiktionary:downloadXMLDump

$(XML_DUMP): $(XML_DUMP_BZ2) $(XML_DUMP_INDEX_BZ2)
	$(GRADLE) processXMLDump $(GRADLE_ARGS) -Ptask=wiktionary.ParseXMLDump -PtaskArgs=$@

$(XML_DUMP_RAW): $(XML_DUMP_BZ2) $(XML_DUMP_INDEX_BZ2)
	$(GRADLE) processXMLDump $(GRADLE_ARGS) -Ptask=ImportXMLDump -PtaskArgs=$@

$(MISPLACED_LABELS): $(XML_DUMP)
	$(GRADLE) processDataFrame -Ptask=wiktionary.tasks.FindMisplacedLabels -PtaskArgs="$< $@" $(GRADLE_ARGS)

publish_misplaced_labels: $(MISPLACED_LABELS)
	$(PYTHON) $(PYTHON_DIR)/publish/publish_misplaced_labels.py $<

$(BASE_DIR)/enwiktionary.sqlite: $(XML_DUMP_BZ2) $(XML_DUMP_INDEX_BZ2)
	$(GRADLE) processXMLDump $(GRADLE_ARGS) -Ptask=wiktionary.tasks.DatabaseImport -PtaskArgs=$@

$(BASE_DIR)/index-xml.txt: $(XML_DUMP_RAW)
	$(GRADLE) processDataFrame $(GRADLE_ARGS) -Ptask=CreateIndexFromXMLDump -PtaskArgs="$< $@"

$(BASE_DIR)/index-html.txt: $(HTML_DUMP_RAW)
	$(GRADLE) processDataFrame $(GRADLE_ARGS) -Ptask=CreateIndexFromHTMLDump -PtaskArgs="$< $@"

$(BASE_DIR)/content_%.ndjson: $(HTML_DUMP)
	$(GRADLE) processDataFrame $(GRADLE_ARGS) -Ptask=wiktionary.tasks.ExtractContent -PtaskArgs="$< $@ $(patsubst content_%.ndjson,%,$(@F))"

.PRECIOUS: $(BASE_DIR)/content_%.ndjson $(BASE_DIR)/wiktionary-$(dump_version)-%.sqlite
$(BASE_DIR)/wiktionary-$(dump_version)-%.sqlite: $(BASE_DIR)/content_%.ndjson
	$(PYTHON) $(PYTHON_DIR)/make_dictionary.py \
		--language $(patsubst wiktionary-$(dump_version)-%.sqlite,%,$(@F)) \
		--dump-version $(dump_version) \
		--index \
		--out $@ \
		$</*.txt

$(BASE_DIR)/wiktionary-$(dump_version)-%.sqlite.lzma: $(BASE_DIR)/wiktionary-$(dump_version)-%.sqlite
	lzma --compress --stdout $< > $@

$(BASE_DIR)/missing-sections.ndjson: $(HTML_DUMP)
	$(GRADLE) processDataFrame -Ptask=wiktionary.tasks.sections.AnalyzeMissingSections -PtaskArgs="$< $@" $(GRADLE_ARGS)

$(BASE_DIR)/header-sections.ndjson: $(HTML_DUMP)
	$(GRADLE) processDataFrame -Ptask=wiktionary.tasks.sections.AnalyzeSectionHeaderNesting -PtaskArgs="$< $@" $(GRADLE_ARGS)

$(BASE_DIR)/filtered_sections.ndjson: $(HTML_DUMP)
	$(GRADLE) processDataFrame -Ptask=wiktionary.tasks.sections.FilterSections -PtaskArgs="$< $@" $(GRADLE_ARGS)

$(BASE_DIR)/stats.ndjson: $(HTML_DUMP)
	$(GRADLE) processDataFrame -Ptask=wiktionary.tasks.stats.ExtractStats -PtaskArgs="$< $@ $(FILTER)" $(GRADLE_ARGS)

$(BASE_DIR)/aggregated_stats.ndjson: $(BASE_DIR)/stats.ndjson
	$(GRADLE) processDataFrame -Ptask=wiktionary.tasks.stats.AggregateStats -PtaskArgs="$< $@" $(GRADLE_ARGS)

shell:
	$(GRADLE) shell --no-daemon --console plain

lint: $(PYTHON)
	$(PYTHON) -m flake8 wiktionary/src/main/python wiktionary/src/test/python

test: test_python

test_python: $(PYTHON)
	PYTHONPATH=wiktionary/src/main/python $(PYTHON) -m unittest discover -s wiktionary/src/test/python

clean:
	rm -rf $(WANTED_ENTRIES) $(AGGREGATED_DIR) $(LINKS) $(INDEX)

# placeholder user-config.py for pywikibot
# https://meta.wikimedia.org/wiki/Special:OAuthConsumerRegistration/propose
define config
family = 'wiktionary'
mylang = 'en'

usernames['wiktionary']['en'] = 'username'
authenticate['en.wiktionary.org'] = ('consumer token',
                                     'consumer_secret',
                                     'access_token',
                                     'access_secret')
endef
export config

user-config.py:
	echo "$$config" > $@
