package digero.wikitext

import digero.model.Template
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.scalatest.matchers.should.Matchers.*
import org.sweble.wikitext.engine.nodes.EngPage
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp
import org.sweble.wikitext.engine.{PageId, PageTitle, WtEngineImpl}
import org.sweble.wikitext.parser.nodes.WtTemplate

import java.util
import scala.jdk.CollectionConverters.*

class TemplateBuilderTest:
  @Test
  def testGetParameter(): Unit = {
    val t = TemplateBuilderTest.parseTemplate("{{foo|a|b}}")
    assertThat(t.getParameters).hasSize(2)
    t.getParameter(1) should contain("a")
    t.getParameter(2) should contain("b")
  }

  @Test
  def testIterable(): Unit = {
    val t = TemplateBuilderTest.parseTemplate("{{foo|a|b}}")
    val parameters = new util.ArrayList[String]
    for (p <- t.asScala) {
      parameters.add(p)
    }
    assertThat(parameters).containsExactly("a", "b")
  }

  @Test
  def testGetParameterByKey(): Unit =
    val t = TemplateBuilderTest.parseTemplate("{{foo|a|b|c=value}}")
    t.getParameter("c") should contain("value")

  @Test
  def testGetParameterByIndex(): Unit =
    val t = TemplateBuilderTest.parseTemplate("{{foo|c=value|a|d=foo|b|}}")
    t.getParameter(1) should contain("a")
    t.getParameter(2) should contain("b")

  @Test
  def testGetNonBlankParameter(): Unit =
    val t = TemplateBuilderTest.parseTemplate("{{foo|a| |c}}")
    t.getNonBlankParameter(1) should contain("a")
    t.getNonBlankParameter(2) shouldBe empty

  @Test
  def testAnonymousParameterOrderingIsPreserved(): Unit =
    val t = TemplateBuilderTest.parseTemplate("{{foo|a|b| |c|foo=bar|d}}")
    assertThat(t.getAnonymousParameters).containsExactly("a", "b", "", "c", "d")

object TemplateBuilderTest:
  class TemplateCollector extends TemplateVisitor:
    final val templates = new util.ArrayList[WtTemplate]
    override def visit(template: WtTemplate): Unit = templates.add(template)

  private def parseTemplate(wikiText: String): Template =
    val page = parse(wikiText)
    val collector = new TemplateBuilderTest.TemplateCollector
    collector.visit(page)
    assertThat(collector.templates).isNotEmpty
    TemplateBuilder.makeTemplate(collector.templates.get(0))


  private def parse(wikiText: String): EngPage =
    val wikiConfig = DefaultConfigEnWp.generate
    val engine = new WtEngineImpl(wikiConfig)
    engine.postprocess(
      new PageId(PageTitle.make(wikiConfig, "test"), 0), wikiText, null
    ).getPage
