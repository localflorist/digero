package digero.wikitext

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.sweble.wikitext.engine.config.WikiConfigImpl
import org.sweble.wikitext.engine.nodes.EngPage
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp
import org.sweble.wikitext.engine.{EngineException, PageId, PageTitle, WtEngineImpl}
import org.sweble.wikitext.parser.nodes.WtTemplate
import org.sweble.wikitext.parser.parser.LinkTargetException

import java.util
import scala.collection.mutable


class TemplateVisitorTest:
  @Test
  def testNestedTemplate(): Unit =
    val parsed = parse("{{template|foo|parameter={{another|russian={{doll}}}}}}")
    val visitor = new TestVisitor
    visitor.visit(parsed)

    assertThat(visitor.foundTemplates.size).isEqualTo(3)

private class TestVisitor extends TemplateVisitor:
  private[wikitext] val foundTemplates = mutable.Buffer[WtTemplate]()

  override def visit(template: WtTemplate): Unit =
    foundTemplates.append(template)
    iterate(template)

private def parse(wikiText: String): EngPage =
  val wikiConfig = DefaultConfigEnWp.generate
  val engine = new WtEngineImpl(wikiConfig)
  engine.postprocess(
    new PageId(PageTitle.make(wikiConfig, "test"), 0), wikiText, null
  ).getPage
