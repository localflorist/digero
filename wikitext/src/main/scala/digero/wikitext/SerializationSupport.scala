package digero.wikitext

import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.{Input, Output}
import com.esotericsoftware.kryo.serializers.{FieldSerializer, JavaSerializer}
import de.fau.cs.osr.ptk.common.ast.{AstLocation, Span}
import org.apache.spark.serializer.KryoRegistrator
import org.objenesis.strategy.StdInstantiatorStrategy
import org.sweble.wikitext.engine.nodes.{EngNowiki, EngPage}
import org.sweble.wikitext.parser.nodes.*
import org.sweble.wikitext.parser.nodes.WtBody.{WtBodyImpl, WtNoBody}
import org.sweble.wikitext.parser.nodes.WtLinkOptions.WtLinkOptionsImpl
import org.sweble.wikitext.parser.nodes.WtLinkTitle.{WtLinkTitleImpl, WtNoLinkTitle}
import org.sweble.wikitext.parser.nodes.WtName.WtNameImpl
import org.sweble.wikitext.parser.nodes.WtTagExtensionBody.{WtNoTagExtensionBody, WtTagExtensionBodyImpl}
import org.sweble.wikitext.parser.nodes.WtTemplateArguments.WtTemplateArgumentsImpl
import org.sweble.wikitext.parser.nodes.WtXmlAttributes.{WtEmptyXmlAttributes, WtXmlAttributesImpl}
import org.sweble.wikitext.parser.{LooksLikeWarning, OddSyntaxWarning, WikitextWarning, WtRtData}

import java.util

object SerializationSupport:
  private val instance = ThreadLocal.withInitial(() => new SerializationSupport)
  def get: SerializationSupport = instance.get


class SerializationSupport extends KryoRegistrator:
  final private val kryo: Kryo = new Kryo()
  final private val input = new Input
  final private val output = new Output(16384, -1)

  kryo.setRegistrationRequired(false)
  kryo.setWarnUnregisteredClasses(true)
  kryo.setInstantiatorStrategy(new Kryo.DefaultInstantiatorStrategy(new StdInstantiatorStrategy))
  registerClasses(kryo)
  kryo.register(classOf[util.ArrayList[_]])
  kryo.register(classOf[util.HashMap[_, _]])
  kryo.register(classOf[util.LinkedList[_]])
  kryo.register(classOf[Array[AnyRef]])
  kryo.register(classOf[Array[Array[AnyRef]]])
  // Log.set(Log.LEVEL_TRACE);

  def serialize(node: WtContentNode): Array[Byte] =
    output.clear()
    kryo.writeClassAndObject(output, node)
    output.toBytes

  def deserialize(bytes: Array[Byte]): WtContentNode =
    input.setBuffer(bytes)
    kryo.readClassAndObject(input).asInstanceOf[WtContentNode]

  override def registerClasses(kryo: Kryo): Unit =
    for (c <- Seq[Class[_]](
        classOf[AstLocation],
        classOf[LooksLikeWarning],
        classOf[WikitextWarning.WarningSeverity],
        classOf[Span],
        classOf[EngPage],
        classOf[EngNowiki],
        classOf[OddSyntaxWarning],
        classOf[WtBody.WtBodyImpl],
        classOf[WtBold], classOf[WtContentNode],
        classOf[WtDefinitionList],
        classOf[WtDefinitionListDef],
        classOf[WtDefinitionListTerm],
        classOf[WtXmlAttributes.WtEmptyXmlAttributes],
        classOf[WtExternalLink],
        classOf[WtHeading],
        classOf[WtHorizontalRule],
        classOf[WtIgnored],
        classOf[WtIllegalCodePoint],
        classOf[WtIllegalCodePoint.IllegalCodePointType],
        classOf[WtImageLink],
        classOf[WtImageLink.ImageViewFormat],
        classOf[WtImageLink.ImageHorizAlign],
        classOf[WtImageLink.ImageVertAlign],
        classOf[WtImEndTag],
        classOf[WtImStartTag],
        classOf[WtInternalLink],
        classOf[WtItalics],
        classOf[WtLctVarConv],
        classOf[WtLinkOptionAltText.WtLinkOptionAltTextImpl],
        classOf[WtLinkOptionGarbage],
        classOf[WtLinkOptionKeyword],
        classOf[WtLinkOptionLinkTarget.WtLinkOptionLinkTargetImpl],
        classOf[WtLinkOptionResize],
        classOf[WtLinkOptions.WtLinkOptionsImpl],
        classOf[WtLinkTarget.LinkTargetType],
        classOf[WtLinkTitle.WtLinkTitleImpl],
        classOf[WtListItem],
        classOf[WtName.WtNameImpl],
        classOf[WtNewline],
        classOf[WtOrderedList],
        classOf[WtPageName],
        classOf[WtParagraph],
        classOf[WtRedirect],
        classOf[WtRtData],
        classOf[WtSection],
        classOf[WtSemiPre],
        classOf[WtSemiPreLine],
        classOf[WtTable],
        classOf[WtTableCaption],
        classOf[WtTableCell],
        classOf[WtTableHeader],
        classOf[WtTableImplicitTableBody],
        classOf[WtTableRow],
        classOf[WtTagExtension],
        classOf[WtTagExtensionBody.WtTagExtensionBodyImpl],
        classOf[WtTemplate],
        classOf[WtTemplateArgument],
        classOf[WtTemplateArguments.WtTemplateArgumentsImpl],
        classOf[WtTemplateParameter],
        classOf[WtText],
        classOf[WtTicks],
        classOf[WtUnorderedList],
        classOf[WtUrl],
        classOf[WtValue.WtValueImpl],
        classOf[WtXmlAttribute],
        classOf[WtXmlAttributeGarbage],
        classOf[WtXmlAttributes.WtXmlAttributesImpl],
        classOf[WtXmlComment],
        classOf[WtXmlElement],
        classOf[WtXmlEmptyTag],
        classOf[WtXmlEndTag],
        classOf[WtXmlCharRef],
        classOf[WtXmlEntityRef],
        classOf[WtXmlStartTag]))

      if classOf[util.Collection[_]].isAssignableFrom(c) then
        kryo.register(c, new FieldSerializer[AnyRef](kryo, c))
      else
        kryo.register(c)

    for (c <- Seq[Class[_]](
        classOf[WtName.WtNoName],
        classOf[WtBody.WtNoBody],
        classOf[WtLinkTitle.WtNoLinkTitle],
        classOf[WtLctFlags.WtNoLctFlags],
        classOf[WtValue.WtNoValue],
        classOf[WtLinkTarget.WtNoLink],
        classOf[WtTagExtensionBody.WtNoTagExtensionBody],
        classOf[WtTemplateArguments.WtEmptyTemplateArguments]))
      kryo.register(c, new JavaSerializer)
