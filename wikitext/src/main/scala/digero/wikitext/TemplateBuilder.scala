package digero.wikitext

import digero.model.Template
import org.sweble.wikitext.parser.nodes
import org.sweble.wikitext.parser.nodes.{WtNode, WtTemplate, WtTemplateArgument}

import WtNodeExtensions.getText
import scala.jdk.CollectionConverters.*

object TemplateBuilder:

  def makeTemplate(template: WtTemplate): Template =
    val parameters = new java.util.LinkedHashMap[String, String]
    var paramIndex = 1
    for
      case argument: WtTemplateArgument <- template.getArgs.asScala.toSeq
    do
      (argument.getName.getText, argument.getValue.getText) match
        case (Some(key), Some(value)) => parameters.put(key, value)
        case (None, value) =>
          parameters.put(paramIndex.toString, value.getOrElse(""))
          paramIndex += 1
        case _ =>

    new Template(template.getName.getAsString, parameters)
