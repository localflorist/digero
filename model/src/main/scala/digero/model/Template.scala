package digero.model

import java.util
import java.util.Map.Entry.comparingByKey
import java.util.function.Consumer
import java.util.stream.{Collectors, Stream}
import java.util.{Collections, Spliterator}

object Template:
  private def apply(entry: util.Map.Entry[String, String]) =
    try
      val index = entry.getKey.toInt
      if (index >= 1) Stream.of(new util.AbstractMap.SimpleEntry[Integer, String](index, entry.getValue))
      else Stream.empty
    catch
      case _: NumberFormatException => Stream.empty

/**
 * @param name       the template name
 * @param parameters parameters in the form of { "1": "first", "3": "foo"}
 */
case class Template(name: String,
                    parameters: util.Map[String, String] = util.Collections.emptyMap())
  extends java.lang.Iterable[String]:

  private val anonymousParameters: util.List[String] = parameters.entrySet
    .stream
    .flatMap(Template.apply)
    .sorted(comparingByKey)
    .map(_.getValue)
    .collect(Collectors.toList)

  def getName: String = name

  def getParameters: util.Map[String, String] = Collections.unmodifiableMap(parameters)

  def getNonBlankParameters: util.List[String] =
    anonymousParameters.stream
      .filter((s: String) => s.trim.nonEmpty)
      .collect(Collectors.toList)

  def getAnonymousParameters = new util.ArrayList[String](anonymousParameters)

  /**
   * @param index 1-based index
   * @return the value, or none
   */
  def getParameter(index: Int): Option[String] =
    assert(index >= 1)
    val value = parameters.get(String.valueOf(index))
    Option(value)

  def getNonBlankParameter(index: Int): Option[String] =
    getParameter(index).filter((s: String) => s.trim.nonEmpty)

  def getParameter(key: String): Option[String] =
    if (parameters.containsKey(key)) Some(parameters.get(key)) else None

  def listIterator: util.ListIterator[String] = anonymousParameters.listIterator

  def listIterator(start: Int): util.ListIterator[String] = anonymousParameters.listIterator(start)

  def iterator: util.Iterator[String] = anonymousParameters.iterator

  override def forEach(consumer: Consumer[_ >: String]): Unit =
    anonymousParameters.forEach(consumer)

  override def spliterator: Spliterator[String] = anonymousParameters.spliterator

  override def toString: String = f"Template{name=$name, parameters=$parameters}"
